FROM openjdk:16-jdk-slim

RUN echo "Updating data of package repositories ..." && \
    apt-get update && \
    echo "Installing package graphviz ..." && \
    apt-get install graphviz --yes