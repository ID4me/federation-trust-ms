/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.denic.openid.federationtrust.business.EntityVertex;
import de.denic.openid.federationtrust.business.FedEntityStmtEdge;
import io.swagger.v3.oas.annotations.media.Schema;
import org.jgrapht.Graph;

import io.micronaut.core.annotation.NonNull;
import javax.annotation.concurrent.Immutable;
import java.util.List;

import static de.denic.openid.federationtrust.endpoint.EdgeResponse.JSONKeys.*;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

@JsonPropertyOrder({SOURCE, TARGET, VALID, ISSUES, MESSAGES})
@Immutable
public final class EdgeResponse {

  private final FedEntityStmtEdge fedEntityStmtEdge;
  private final EntityVertex source, target;

  public EdgeResponse(@NonNull final FedEntityStmtEdge fedEntityStmtEdge,
                      @NonNull final Graph<EntityVertex, FedEntityStmtEdge> graph) {
    this.fedEntityStmtEdge = requireNonNull(fedEntityStmtEdge, "Missing Federation Entity Stmt edge");
    this.source = requireNonNull(graph.getEdgeSource(this.fedEntityStmtEdge), "Missing source");
    this.target = requireNonNull(graph.getEdgeTarget(this.fedEntityStmtEdge), "Missing target");
  }

  @Schema(name = SOURCE,
          description = "Source node/vertex of this edge.",
          required = true)
  @NonNull
  @JsonGetter(SOURCE)
  public String getSource() {
    return source.getEntity().getPlainValue();
  }

  @Schema(name = TARGET,
          description = "Target node/vertex of this edge.",
          required = true)
  @NonNull
  @JsonGetter(TARGET)
  public String getTarget() {
    return target.getEntity().getPlainValue();
  }

  @Schema(name = VALID,
          description = "Whether this edge is valid.",
          required = true)
  @JsonGetter(VALID)
  public boolean isValid() {
    return fedEntityStmtEdge.isValid();
  }

  @Schema(name = ISSUES,
          description = "Human readable severe issues of link between Entity and Authority Hint.")
  @NonNull
  @JsonGetter(ISSUES)
  public List<String> getIssues() {
    return fedEntityStmtEdge.getIssues();
  }

  @Schema(name = MESSAGES,
          description = "Human readable further messages regarding link between Entity and Authority Hint.")
  @NonNull
  @JsonGetter(MESSAGES)
  public List<String> getMessages() {
    return fedEntityStmtEdge.getMessages();
  }

  @NonNull
  public static List<EdgeResponse> of(final Graph<EntityVertex, FedEntityStmtEdge> graph) {
    return requireNonNull(graph, "Missing graph").edgeSet()
            .stream()
            .map(fedEntityStmtEdge -> new EdgeResponse(fedEntityStmtEdge, graph))
            .collect(toList());
  }

  static final class JSONKeys {

    static final String SOURCE = "source";
    static final String TARGET = "target";
    static final String VALID = "valid";
    static final String ISSUES = "issues";
    static final String MESSAGES = "messages";

  }

}
