/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint;

import de.denic.openid.federationtrust.business.EntityVertex;
import de.denic.openid.federationtrust.business.FedEntityStmtEdge;
import de.denic.openid.federationtrust.business.FederationTrustService;
import de.denic.openid.federationtrust.business.TrustGraph;
import de.denic.openid.federationtrust.common.Entity;
import de.denic.openid.federationtrust.common.TrustAnchor;
import guru.nidi.graphviz.attribute.Color;
import guru.nidi.graphviz.attribute.Style;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.parse.Parser;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.http.server.types.files.StreamedFile;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.dot.DOTExporter;
import org.slf4j.MDC;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.io.*;
import java.time.ZonedDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

import static de.denic.openid.federationtrust.business.FederationTrustService.*;
import static guru.nidi.graphviz.engine.Format.SVG_STANDALONE;
import static io.micronaut.http.HttpHeaders.CACHE_CONTROL;
import static io.micronaut.http.HttpHeaders.EXPIRES;
import static io.micronaut.http.HttpStatus.NOT_FOUND;
import static io.micronaut.http.HttpStatus.OK;
import static io.micronaut.http.MediaType.APPLICATION_JSON;
import static java.time.ZonedDateTime.now;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Objects.requireNonNull;

@Immutable
@ExecuteOn(TaskExecutors.IO)
@Controller
public final class TrustGraphController {

  private static final String TEXT_VND_GRAPHVIZ = "text/vnd.graphviz";
  private static final String IMAGE_SVG_XML = "image/svg+xml";
  private static final MediaType IMAGE_SVG_XML_TYPE = MediaType.of(IMAGE_SVG_XML);
  private static final Attribute LEAF_ENTITY_COLOR_ATTRIBUTE = DefaultAttribute.createAttribute("skyblue");
  private static final Attribute TRUST_ANCHOR_COLOR_ATTRIBUTE = DefaultAttribute.createAttribute("palegreen");
  private static final Attribute VALID_EDGE_COLOR_ATTRIBUTE = DefaultAttribute.createAttribute("green");
  private static final Attribute INVALID_EDGE_COLOR_ATTRIBUTE = DefaultAttribute.createAttribute("red");
  private static final Attribute ENTITY_WITH_ISSUES_COLOR_ATTRIBUTE = INVALID_EDGE_COLOR_ATTRIBUTE;

  private final FederationTrustService federationTrustService;

  public TrustGraphController(@NonNull final FederationTrustService federationTrustService) {
    this.federationTrustService = requireNonNull(federationTrustService, "Missing Federation Trust Service component");
  }

  @Operation(summary = "Build and verify Trust Graph of an Entity",
          description = "Based on <a href=\"https://openid.net/specs/openid-connect-federation-1_0.html\">OpenID Connect Federation Specification's</a> draft 12. " +
                  "Builds graph of Trust relations of a given Entity up to Trust Anchors (at least 'generic' ones providing no Authority Hints). " +
                  "You can declare one custom Trust Anchor entity to apply, " +
                  "optionally together with its public <a href=\"https://tools.ietf.org/html/rfc7517#section-5\">JWK set (RFC7517)</a>."
  )
  @ApiResponse(responseCode = "200",
          description = "Trust Graph reached Trust Anchor, and its validation succeeds.",
          content = @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = TrustGraphResponse.class)
          ))
  @ApiResponse(responseCode = "404",
          description = "Trust Chain reached no Trust Anchor, or its validation failed.",
          content = @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = TrustGraphResponse.class)
          ))
  @Get(uri = "/trust/graph/{entity}{?trust-anchor,ta-jwkset,max-path-length}", produces = APPLICATION_JSON)
  public HttpResponse<TrustGraphResponse> get(@Parameter(description = "Entity URI (not its HTTP(S) URL!)", example = "interested.in/entity")
                                              @NonNull @PathVariable("entity") final String host,
                                              @Parameter(description = "Explicit/custom Trust Anchor entity URI (not its HTTP(S) URL!)", example = "famous.trust/anchor")
                                              @Nullable @QueryValue("trust-anchor") final String trustAnchorValue,
                                              @Parameter(description = "RFC7517-compliant JWK set of given Trust Anchor, as plain OR base64-encoded JSON.", example = "{\"keys\":[...]} OR eyJrZXlzIjpb...")
                                              @Nullable @QueryValue("ta-jwkset") final String trustAnchorJWKSetValue,
                                              @Schema(description = "Custom max path length (default '" + DEFAULT_MAX_PATH_LENGTH + "').", maximum = "" + ALLOWED_MAX_OF_MAX_PATH_LENGTH)
                                              @Nullable @QueryValue("max-path-length") final Integer maxPathLength) {
    return getHttpResponseFrom(host, trustAnchorValue, trustAnchorJWKSetValue, maxPathLength, TrustGraphResponse::new);
  }

  @Hidden
  @Get(uri = "/trust/graph-dot/{entity}{?trust-anchor,ta-jwkset,max-path-length}", produces = TEXT_VND_GRAPHVIZ)
  public HttpResponse<String> getResultAsGraphVizDot(@NonNull @PathVariable("entity") final String host,
                                                     @Nullable @QueryValue("trust-anchor") final String trustAnchorValue,
                                                     @Nullable @QueryValue("ta-jwkset") final String trustAnchorJWKSetValue,
                                                     @Nullable @QueryValue("max-path-length") final Integer maxPathLength) {
    return getHttpResponseFrom(host, trustAnchorValue, trustAnchorJWKSetValue, maxPathLength, TrustGraphController::renderAsGraphVizDot);
  }

  @NonNull
  private <TYPE> MutableHttpResponse<TYPE> getHttpResponseFrom(final @NonNull String host,
                                                               final @Nullable String trustAnchorValue,
                                                               final @Nullable String trustAnchorJWKSetValue,
                                                               final @Nullable Integer maxPathLength,
                                                               final @NonNull Function<TrustGraph, TYPE> trustGraphConverter) {
    final Entity entity = Entity.ofHost(host);
    MDC.put(MDC_LEAF, entity.getPlainValue());
    try {
      final TrustGraph trustGraph = getTrustGraphWith(trustAnchorValue, trustAnchorJWKSetValue, maxPathLength, entity);
      final boolean overallSuccess = trustGraph.isTrustAnchorReached() && trustGraph.verificationHasSucceeded();
      final ZonedDateTime endOfValidityInterval = trustGraph.getValidityInterval().getEnd().orElse(now());
      final MutableHttpResponse<String> response = HttpResponse.status(overallSuccess ? OK : NOT_FOUND);
      response.getHeaders()
              .add(EXPIRES, endOfValidityInterval)
              .set(CACHE_CONTROL, (overallSuccess ? "max-age=" + now().until(endOfValidityInterval, SECONDS)
                      : "no-cache"));
      return response.body(requireNonNull(trustGraphConverter, "Missing converter").apply(trustGraph));
    } finally {
      MDC.clear();
    }
  }

  @Hidden
  @Get(uri = "/trust/graph-image/{entity}{?trust-anchor,ta-jwkset,max-path-length}", produces = IMAGE_SVG_XML)
  public StreamedFile getResultAsImage(@NonNull @PathVariable("entity") final String host,
                                       @Nullable @QueryValue("trust-anchor") final String trustAnchorValue,
                                       @Nullable @QueryValue("ta-jwkset") final String trustAnchorJWKSetValue,
                                       @Nullable @QueryValue("max-path-length") final Integer maxPathLength) {
    final Entity entity = Entity.ofHost(host);
    MDC.put(MDC_LEAF, entity.getPlainValue());
    try {
      final TrustGraph trustGraph = getTrustGraphWith(trustAnchorValue, trustAnchorJWKSetValue, maxPathLength, entity);
      return new StreamedFile(renderGraphVizDotAsSVG(renderAsGraphVizDot(trustGraph)), IMAGE_SVG_XML_TYPE);
    } finally {
      MDC.clear();
    }
  }

  @NonNull
  private TrustGraph getTrustGraphWith(final @Nullable String trustAnchorValue,
                                       final @Nullable String trustAnchorJWKSetValue,
                                       final @Nullable Integer maxPathLength,
                                       final Entity entity) {
    final TrustAnchor trustAnchor = TrustAnchor.fromEntityAndJWKSet(trustAnchorValue, trustAnchorJWKSetValue);
    return federationTrustService.seekForChainOf(entity)
            .withMaxPathLength(maxPathLength)
            .withCustomTrustAnchors(trustAnchor)
            .graphNow();
  }

  @NonNull
  public static String renderAsGraphVizDot(@NonNull final TrustGraph trustGraph) {
    final DOTExporter<EntityVertex, FedEntityStmtEdge> exporter =
            new DOTExporter<>(entityVertex -> "\"" + entityVertex.getEntity().getPlainValue() + "\"");
    exporter.setVertexAttributeProvider(entityVertex -> {
      final Map<String, Attribute> map = new LinkedHashMap<>();
      map.put("label", DefaultAttribute.createAttribute(entityVertex.getEntity().getPlainValue()));
      if (trustGraph.getLeafVertex().equals(entityVertex)) {
        map.put("color", LEAF_ENTITY_COLOR_ATTRIBUTE);
      } else if (entityVertex.isTrustAnchor()) {
        map.put("color", TRUST_ANCHOR_COLOR_ATTRIBUTE);
      } else if (entityVertex.hasIssues()) {
        map.put("color", ENTITY_WITH_ISSUES_COLOR_ATTRIBUTE);
      }
      return map;
    });
    exporter.setEdgeAttributeProvider(fedEntityStmtEdge ->
            Map.of("color", fedEntityStmtEdge.isValid() ? VALID_EDGE_COLOR_ATTRIBUTE : INVALID_EDGE_COLOR_ATTRIBUTE));
    final Writer result = new StringWriter();
    exporter.exportGraph(trustGraph.getGraph(), result);
    return result.toString();
  }

  @NonNull
  public static InputStream renderGraphVizDotAsSVG(@NonNull final String dotDescription) {
    final MutableGraph dotParsedGraph;
    try (final StringReader dotDescriptionReader = new StringReader(dotDescription)) {
      dotParsedGraph = new Parser().read(dotDescriptionReader, "GRAPH");
    } catch (final IOException e) {
      throw new RuntimeException("Internal error!", e);
    }

    final MutableGraph mutableGraph = dotParsedGraph.graphAttrs()
            .add(Color.rgb("bbbbbb").gradient(Color.rgb("eeeeee")).background().angle(90))
            .nodeAttrs().add(Color.WHITE.fill());
    mutableGraph.nodes()
            .forEach(node -> node.add(Style.combine(Style.lineWidth(2),Style.FILLED)));
    try (final ByteArrayOutputStream target = new ByteArrayOutputStream()) {
      Graphviz.fromGraph(dotParsedGraph)
              .width(1200)
              .render(SVG_STANDALONE)
              .toOutputStream(target);
      return new ByteArrayInputStream(target.toByteArray());
    } catch (final IOException e) {
      throw new RuntimeException("Internal error!", e);
    }
  }

}
