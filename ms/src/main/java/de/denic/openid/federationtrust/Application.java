/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;

import static io.reactivex.plugins.RxJavaPlugins.setErrorHandler;

@OpenAPIDefinition(
        info = @Info(
                title = "OpenID Connect Federation Trust Service",
                version = "0.1",
                description = "API validates Federation Trust Chain of an Entity, optionally regarding an explicit Trust Anchor (with its JWK set)",
                license = @License(name = "The MIT License (MIT)", url = "https://opensource.org/licenses/mit-license.php"),
                contact = @Contact(url = "https://www.denic.de/service/denic-id/", name = "DENIC eG Team", email = "team-denic-id@denic.de")
        )
)
@Immutable
public final class Application {

  private static final Logger LOG = LoggerFactory.getLogger(Application.class);

  public static void main(String[] args) {
    // Following recommendation from https://github.com/ReactiveX/RxJava/wiki/What's-different-in-2.0#error-handling:
    setErrorHandler(uncaughtThrowable -> {
      final Throwable cause = uncaughtThrowable.getCause();
      if (cause == null) {
        LOG.info("Caught unhandled: '{}' ({})", uncaughtThrowable.getMessage(), uncaughtThrowable.getClass());
      } else {
        LOG.info("Caught unhandled: '{}' ({}), caused by '{}' ({})",
                uncaughtThrowable.getMessage(), uncaughtThrowable.getClass(), cause.getMessage(), cause.getClass());
      }
    });
    Micronaut.run(Application.class);
  }

}
