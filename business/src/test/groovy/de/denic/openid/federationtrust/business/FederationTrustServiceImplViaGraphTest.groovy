/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business

import com.nimbusds.jose.util.Base64
import de.denic.openid.federationtrust.common.Entity
import de.denic.openid.federationtrust.wellknown.FederationEntityConfigClient
import io.micronaut.http.client.BlockingHttpClient
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import spock.lang.Specification

import static de.denic.openid.federationtrust.common.TrustAnchor.fromEntity
import static io.micronaut.http.HttpResponse.notFound

class FederationTrustServiceImplViaGraphTest extends Specification {

  HttpClient httpClientMock = Mock()
  BlockingHttpClient blockingHttpClientMock = Mock()
  FederationTrustService CUT

  def setup() {
    FederationEntityConfigClient fedEntityConfigInfoClient = new FederationEntityConfigClient.Impl(httpClientMock)
    FederationEntityStmtClient fedEntityStmtClient = new FederationEntityStmtClient.Impl(httpClientMock, fedEntityConfigInfoClient)
    CUT = new FederationTrustService.Impl(fedEntityConfigInfoClient, fedEntityStmtClient)
    _ * httpClientMock.toBlocking() >> blockingHttpClientMock
  }

  def 'Leaf entity provides NO Federation Entity Config'() {
    given:
    Entity leaf = Entity.ofHost('leaf.without.config')

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(leaf).graphNow()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> { throw new HttpClientResponseException('Not found', notFound()) }
    with(trustGraph.graph) {
      it.edgeSet().isEmpty()
      it.vertexSet().size() == 1
      it.vertexSet().getAt(0) == trustGraph.leafVertex
    }
    with(trustGraph.leafVertex) {
      it.issues == ['NO Federation Entity Config available.']
      it.messages.isEmpty()
      it.isNoTrustAnchor()
      it.authorityHintsToFollow.isEmpty()
      !it.isValid()
    }
    !trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  def "Leaf entity provides EXPIRED Federation Entity Config Info"() {
    given:
    Entity leaf = Entity.ofHost('leaf.config.expired')

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(leaf).graphNow()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(leaf.asHttpsURI().getHost(), """\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"irrelevant",
  "iat":1262304000,
  "exp":1420070400,
  "jwks":{
    "keys":[]
  }
}""")
    with(trustGraph.graph) {
      it.edgeSet().isEmpty()
      it.vertexSet().size() == 1
    }
    with(trustGraph.leafVertex) {
      it.issues == ['Federation Entity Config currently invalid: [2010-01-01T00:00Z[UTC],2015-01-01T00:00Z[UTC]].']
      it.messages.isEmpty()
      it.isNoTrustAnchor()
      it.authorityHintsToFollow.isEmpty()
      !it.isValid()
    }
    !trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  def 'Querying for Leaf entity also beeing custom Trust Anchor'() {
    given:
    Entity leaf = Entity.ofHost('leaf.also.custom.trust.anchor')
    def customTrustAnchor = fromEntity(leaf)
    Entity authorityHint = Entity.ofURI(URI.create('https://no.trust.anchor'))

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(leaf)
            .withCustomTrustAnchors(customTrustAnchor)
            .graphNow()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(leaf.asHttpsURI().getHost(), """\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks":{
    "keys":[]
  },
  "authority_hints": [
    "${authorityHint}"
  ]
}""")
    with(trustGraph.graph) {
      it.edgeSet().isEmpty()
      it.vertexSet().size() == 1
    }
    with(trustGraph.leafVertex) {
      it.issues == ["Verification of self-signed Federation Entity Configuration failed: JWS' header '{\"kid\":\"leaf.also.custom.trust.anchor-kid\",\"alg\":\"HS256\"}' references NO JWK from its self-signed payload."]
      it.messages == ['Reached custom Trust Anchor.']
      it.isTrustAnchor()
      it.authorityHintsToFollow.isEmpty()
      !it.isValid()
    }
    trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  def 'Querying for Leaf entity also beeing Trust Anchor (having NO authority hints)'() {
    given:
    Entity leaf = Entity.ofHost('leaf.also.trust.anchor')

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(leaf).graphNow()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(leaf.asHttpsURI().getHost(), """\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks":{
    "keys":[]
  },
  "authority_hints": []
}""")
    with(trustGraph.graph) {
      it.edgeSet().isEmpty()
      it.vertexSet().size() == 1
    }
    with(trustGraph.leafVertex) {
      it.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"leaf.also.trust.anchor-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
      it.messages == ['Reached generic Trust Anchor.']
      it.isTrustAnchor()
      it.authorityHintsToFollow.isEmpty()
      !it.isValid()
    }
    trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  def 'Querying for Entity showing single Authority Hint being custom Trust Anchor'() {
    given:
    Entity leaf = Entity.ofHost('leaf.with.auth-hint')
    Entity trustAnchor = Entity.ofHost('trust.anchor')
    URI trustAnchorsFederationAPI = URI.create('https://trust.anchor/federation_api_endpoint')

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(leaf).graphNow()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(leaf.asHttpsURI().getHost(), """\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${trustAnchor.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchor.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(trustAnchor.asHttpsURI().getHost(), """\
{
  "iss":"${trustAnchor.asHttpsURI()}",
  "sub":"${trustAnchor.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${trustAnchorsFederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  },
  "authority_hints": []
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchorsFederationAPI}?sub=https%3A%2F%2Fleaf.with.auth-hint&iss=https%3A%2F%2Ftrust.anchor" })
            >> dummyJWSWithPayload(trustAnchor.asHttpsURI().getHost(), """\
{
  "iss":"${trustAnchor.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": []
}""")
    println trustGraph
    with(trustGraph.graph) {
      with(it.vertexSet()) {
        it.size() == 2
        it.getAt(0) == trustGraph.leafVertex
        with(it.getAt(1)) {
          it.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"trust.anchor-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
          it.messages == ['Reached generic Trust Anchor.']
          it.isTrustAnchor()
          it.authorityHintsToFollow.isEmpty()
          !it.isValid()
        }
      }
      with(it.edgeSet().sort()) {
        it.size() == 1
        with(it.get(0)) {
          it.isLinking(leaf, trustAnchor)
          it.issues.isEmpty()
          it.messages.isEmpty()
          !it.isValid()
        }
      }
    }
    with(trustGraph.leafVertex) {
      it.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"leaf.with.auth-hint-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
      it.messages.isEmpty()
      it.isNoTrustAnchor()
      it.authorityHintsToFollow.isEmpty()
      !it.isValid()
    }
    trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  def 'Querying for Entity showing single Authority Hint having max-path-length of MINUS ONE'() {
    given:
    Entity leaf = Entity.ofHost('leaf.with.auth-hint')
    Entity trustAnchor = Entity.ofHost('trust.anchor')
    URI trustAnchorsFederationAPI = URI.create('https://trust.anchor/federation_api_endpoint')

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(leaf).graphNow()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(leaf.asHttpsURI().getHost(), """\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${trustAnchor.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchor.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(trustAnchor.asHttpsURI().getHost(), """\
{
  "iss":"${trustAnchor.asHttpsURI()}",
  "sub":"${trustAnchor.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${trustAnchorsFederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  },
  "constraints": {
    "max_path_length": -1
  },
  "authority_hints": [
    "foo.bar.baz"
  ]
}""")
    println trustGraph
    with(trustGraph.graph) {
      it.edgeSet().isEmpty()
      with(it.vertexSet()) {
        it.size() == 2
        it.getAt(0) == trustGraph.leafVertex
        with(it.getAt(1)) {
          it.issues == ['Current path length exceeds Entity\'s declared max. path length: 0 > -1.']
          it.messages.isEmpty()
          it.isNoTrustAnchor()
          it.authorityHintsToFollow.isEmpty()
          !it.isValid()
        }
      }
    }
    with(trustGraph.leafVertex) {
      it.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"leaf.with.auth-hint-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
      it.messages.isEmpty()
      it.isNoTrustAnchor()
      it.authorityHintsToFollow.isEmpty()
      !it.isValid()
    }
    !trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  def 'Querying for Entity showing two Authority Hints, both linked to same common Trust Anchor'() {
    given:
    Entity leaf = Entity.ofHost('leaf')
    Entity firstAuthHint = Entity.ofHost('first.auth.hint')
    Entity secondAuthHint = Entity.ofHost('second.auth.hint')
    Entity trustAnchor = Entity.ofHost('trust.anchor')
    URI authHint1FederationAPI = URI.create("${firstAuthHint.asHttpsURI()}/federation_api_endpoint")
    URI authHint2FederationAPI = URI.create("${secondAuthHint.asHttpsURI()}/federation_api_endpoint")
    URI trustAnchorsFederationAPI = URI.create("${trustAnchor.asHttpsURI()}/federation_api_endpoint")

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(leaf).graphNow()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(leaf.asHttpsURI().getHost(), """\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${firstAuthHint.asHttpsURI()}","${secondAuthHint.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${firstAuthHint.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(firstAuthHint.asHttpsURI().getHost(), """\
{
  "iss":"${firstAuthHint.asHttpsURI()}",
  "sub":"${firstAuthHint.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${authHint1FederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${trustAnchor.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${secondAuthHint.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(secondAuthHint.asHttpsURI().getHost(), """\
{
  "iss":"${secondAuthHint.asHttpsURI()}",
  "sub":"${secondAuthHint.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${authHint2FederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${trustAnchor.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchor.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(trustAnchor.asHttpsURI().getHost(), """\
{
  "iss":"${trustAnchor.asHttpsURI()}",
  "sub":"${trustAnchor.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${trustAnchorsFederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  },
  "authority_hints": []
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${authHint1FederationAPI}?sub=https%3A%2F%2F${leaf}&iss=https%3A%2F%2F${firstAuthHint}" })
            >> dummyJWSWithPayload(firstAuthHint.asHttpsURI().getHost(), """\
{
  "iss":"${firstAuthHint.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  }
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${authHint2FederationAPI}?sub=https%3A%2F%2F${leaf}&iss=https%3A%2F%2F${secondAuthHint}" })
            >> dummyJWSWithPayload(secondAuthHint.asHttpsURI().getHost(), """\
{
  "iss":"${secondAuthHint.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  }
}""")
    // Hint: One of both following Federation Entity Statement query is NOT issued because the Trust Anchor as its issuer is already checked, and this check fails!
    (_..1) * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchorsFederationAPI}?sub=https%3A%2F%2F${firstAuthHint}&iss=https%3A%2F%2F${trustAnchor}" })
            >> dummyJWSWithPayload(trustAnchor.asHttpsURI().getHost(), """\
{
  "iss":"${trustAnchor.asHttpsURI()}",
  "sub":"${firstAuthHint.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  }
}""")
    (_..1) * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchorsFederationAPI}?sub=https%3A%2F%2F${secondAuthHint}&iss=https%3A%2F%2F${trustAnchor}" })
            >> dummyJWSWithPayload(trustAnchor.asHttpsURI().getHost(), """\
{
  "iss":"${trustAnchor.asHttpsURI()}",
  "sub":"${secondAuthHint.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  }
}""")
    println trustGraph
    with(trustGraph.graph) {
      with(it.vertexSet().sort()) {
        it.size() == 4
        with(it.getAt(0)) { EntityVertex firstAuthHintNode ->
          firstAuthHintNode.entity == firstAuthHint
          firstAuthHintNode.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"first.auth.hint-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
          firstAuthHintNode.messages.isEmpty()
          firstAuthHintNode.isNoTrustAnchor()
          firstAuthHintNode.authorityHintsToFollow.isEmpty()
          !firstAuthHintNode.isValid()
        }
        it.getAt(1) == trustGraph.leafVertex
        with(it.getAt(2)) { EntityVertex secondAuthHintNode ->
          secondAuthHintNode.entity == secondAuthHint
          secondAuthHintNode.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"second.auth.hint-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
          secondAuthHintNode.messages.isEmpty()
          secondAuthHintNode.isNoTrustAnchor()
          secondAuthHintNode.authorityHintsToFollow.isEmpty()
          !secondAuthHintNode.isValid()
        }
        with(it.getAt(3)) { EntityVertex trustAnchorNode ->
          trustAnchorNode.entity == trustAnchor
          trustAnchorNode.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"trust.anchor-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
          trustAnchorNode.messages == ['Reached generic Trust Anchor.']
          trustAnchorNode.isTrustAnchor()
          trustAnchorNode.authorityHintsToFollow.isEmpty()
          !trustAnchorNode.isValid()
        }
      }
      with(it.edgeSet().sort()) {
        it.size() == 4
        with(it.get(0)) {
          it.isLinking(leaf, firstAuthHint)
          it.messages.isEmpty()
          it.issues.isEmpty()
        }
        with(it.get(1)) {
          it.isLinking(leaf, secondAuthHint)
          it.messages.isEmpty()
          it.issues.isEmpty()
        }
        with(it.get(2)) {
          it.isLinking(firstAuthHint, trustAnchor)
          it.messages.isEmpty()
          it.issues.isEmpty()
        }
        with(it.get(3)) {
          it.isLinking(secondAuthHint, trustAnchor)
          it.messages.isEmpty()
          it.issues.isEmpty()
        }
      }
    }
    with(trustGraph.leafVertex) {
      it.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"leaf-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
      it.messages.isEmpty()
      it.isNoTrustAnchor()
      it.authorityHintsToFollow.isEmpty()
      !it.isValid()
    }
    trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  def 'Querying for Entity declaring itself as Authority Hint'() {
    given:
    Entity leaf = Entity.ofHost('leaf')
    Entity selfAuthHint = Entity.ofURI(leaf.asHttpsURI())

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(leaf).graphNow()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(leaf.asHttpsURI().getHost(), """\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${selfAuthHint.asHttpsURI()}"
  ]
}""")
    println trustGraph
    with(trustGraph.graph) {
      it.edgeSet().isEmpty()
      with(it.vertexSet().sort()) {
        it.size() == 1
        it.getAt(0) == trustGraph.leafVertex
      }
    }
    with(trustGraph.leafVertex) {
      it.issues.containsAll(['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"leaf-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.'])
      it.messages.isEmpty()
      it.isNoTrustAnchor()
      it.authorityHintsToFollow.isEmpty()
      !it.isValid()
    }
    !trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  def 'Querying for Entity with two Authority Hints, one misses Federation API URI'() {
    given:
    Entity leaf = Entity.ofHost('leaf')
    Entity apiAuthHint = Entity.ofHost('api.auth.hint')
    Entity noAPIAuthHint = Entity.ofHost('no-api.auth.hint')
    URI validAuthHintFederationAPI = URI.create("${apiAuthHint.asHttpsURI()}/federation_api_endpoint")

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(leaf).graphNow()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(leaf.asHttpsURI().getHost(), """\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${apiAuthHint.asHttpsURI()}","${noAPIAuthHint.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${apiAuthHint.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(apiAuthHint.asHttpsURI().getHost(), """\
{
  "iss":"${apiAuthHint.asHttpsURI()}",
  "sub":"${apiAuthHint.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${validAuthHintFederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  }
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${noAPIAuthHint.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload(noAPIAuthHint.asHttpsURI().getHost(), """\
{
  "iss":"${noAPIAuthHint.asHttpsURI()}",
  "sub":"${noAPIAuthHint.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  }
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${validAuthHintFederationAPI}?sub=https%3A%2F%2F${leaf}&iss=https%3A%2F%2F${apiAuthHint}" })
            >> dummyJWSWithPayload(apiAuthHint.asHttpsURI().getHost(), """\
{
  "iss":"${apiAuthHint.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  }
}""")
    println trustGraph
    with(trustGraph.graph) {
      with(it.vertexSet().sort()) {
        it.size() == 3
        with(it.getAt(0)) { EntityVertex apiAuthHintNode ->
          apiAuthHintNode.entity == apiAuthHint
          apiAuthHintNode.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"api.auth.hint-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
          apiAuthHintNode.messages == ['Reached generic Trust Anchor.']
          apiAuthHintNode.isTrustAnchor()
          apiAuthHintNode.authorityHintsToFollow.isEmpty()
          !apiAuthHintNode.isValid()
        }
        it.getAt(1) == trustGraph.leafVertex
        with(it.getAt(2)) { EntityVertex noAPIAuthHintNode ->
          noAPIAuthHintNode.entity == noAPIAuthHint
          noAPIAuthHintNode.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"no-api.auth.hint-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
          noAPIAuthHintNode.messages == ['Reached generic Trust Anchor.']
          noAPIAuthHintNode.isTrustAnchor()
          noAPIAuthHintNode.authorityHintsToFollow.isEmpty()
          !noAPIAuthHintNode.isValid()
        }
      }
      with(it.edgeSet().sort()) {
        it.size() == 2
        with(it.get(0)) {
          it.isLinking(leaf, apiAuthHint)
          it.issues.isEmpty()
          it.messages.isEmpty()
          !it.isValid()
        }
        with(it.get(1)) {
          it.isLinking(leaf, noAPIAuthHint)
          it.issues == ["${noAPIAuthHint.asHttpsURI()}: Missing Federation API Endpoint." as String]
          it.messages.isEmpty()
          !it.isValid()
        }
      }
    }
    with(trustGraph.leafVertex) {
      it.issues == ['Verification of self-signed Federation Entity Configuration failed: JWS\' header \'{"kid":"leaf-kid","alg":"HS256"}\' references NO JWK from its self-signed payload.']
      it.messages.isEmpty()
      it.isNoTrustAnchor()
      it.authorityHintsToFollow.isEmpty()
      !it.isValid()
    }
    trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  static String dummyJWSWithPayload(String keyIdPrefix, String payload) {
    def dummyHeader = "{\"kid\":\"${keyIdPrefix}-kid\",\"alg\":\"HS256\"}"
    def dummySignature = 'DummySignature'
    "${Base64.encode(dummyHeader)}.${Base64.encode(payload)}.${dummySignature}"
  }

}
