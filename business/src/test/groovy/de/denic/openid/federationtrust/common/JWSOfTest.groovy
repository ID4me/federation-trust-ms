/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.common

import com.nimbusds.jose.*
import com.nimbusds.jose.crypto.ECDSASigner
import com.nimbusds.jose.jwk.Curve
import com.nimbusds.jose.jwk.ECKey
import com.nimbusds.jose.jwk.gen.ECKeyGenerator
import de.denic.openid.federationtrust.business.FederationEntityStmt
import spock.lang.Specification

import static JWSOf.from

class JWSOfTest extends Specification {

  static final ECKey EC_JWK = new ECKeyGenerator(Curve.P_256)
          .keyID("test-key")
          .generate()
  static final ECKey EC_PUBLIC_JWK = EC_JWK.toPublicJWK()
  static final JWSSigner SIGNER = new ECDSASigner(EC_JWK)
  // Following payload valid from '2020-01-01T00:00:00Z' till '2100-01-01T00:00:00Z':
  static final String TEST_PAYLOAD = """
          {
            "iss": "https://issuer.uri",
            "sub": "https://subject.uri",
            "iat": 1577836800,
            "exp": 4102444800,
            "jwks": {
              "keys": [
                ${EC_PUBLIC_JWK.toJSONString()}
              ]
            }
          }"""

  def 'SUCCESSFUL verfication of correctly self-signed JWS'() {
    given:
    JWSObject jwsObject = new JWSObject(
            new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(EC_JWK.getKeyID()).build(),
            new Payload(TEST_PAYLOAD))
    jwsObject.sign(SIGNER)
    def CUT = from(jwsObject.serialize(), FederationEntityStmt)

    expect:
    CUT.verifySelfSigned()
  }

  def 'Verfication of payload-modified self-signed JWS FAILED'() {
    given:
    JWSObject correctJwsObject = new JWSObject(
            new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(EC_JWK.getKeyID()).build(),
            new Payload(TEST_PAYLOAD))
    correctJwsObject.sign(SIGNER)
    def modifiedPayload = TEST_PAYLOAD.replace('subject', 'tcejbus')
    JWSObject payloadModifiedJwsObject = new JWSObject(correctJwsObject.header.toBase64URL(),
            new Payload(modifiedPayload),
            correctJwsObject.signature)
    def CUT = from(payloadModifiedJwsObject.serialize(), FederationEntityStmt)

    expect:
    !CUT.verifySelfSigned()
  }

}
