/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business

import de.denic.openid.federationtrust.common.Entity
import io.micronaut.http.client.HttpClient
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Requires
import spock.lang.Specification

import javax.inject.Inject

import static de.denic.openid.federationtrust.util.Utils.isHTTPsReachable
import static de.denic.openid.federationtrust.util.Utils.isURLReachable

@MicronautTest
class FederationEntityStmtClientIntegrationTest extends Specification {

  @Inject
  HttpClient httpClient
  FederationEntityStmtClient CUT

  def setup() {
    CUT = new FederationEntityStmtClient.Impl(httpClient)
  }

  def "Empty result querying Entity 'example.com' not having Federation Well-Known endpoint"() {
    expect:
    CUT.queryIssuer(Entity.ofHost('example.com'))
            .forStmtRegardingSubject(Entity.ofHost('foo.bar'))
            .isEmpty()
  }

  @Requires({ isHTTPsReachable('fapi.c2id.com') })
  def "Empty result querying Entity 'fapi.c2id.com' not providing Federation API endpoint with Well-Known response"() {
    expect:
    CUT.queryIssuer(Entity.ofHost('fapi.c2id.com'))
            .forStmtRegardingSubject(Entity.ofHost('foo.bar'))
            .isEmpty()
  }

  @Requires({ isURLReachable('https://oidcfed.igtf.net/.well-known/openid-configuration') })
  def "It's illegal to query same Entity 'oidcfed.igtf.net' as Issuer and Subject as well"() {
    when:
    CUT.queryIssuer(Entity.ofHost('oidcfed.igtf.net'))
            .forStmtRegardingSubject(Entity.ofHost('oidcfed.igtf.net'))

    then:
    thrown(IllegalArgumentException)
  }

}
