/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.common

import spock.lang.Specification

import java.time.ZonedDateTime

import static de.denic.openid.federationtrust.common.Interval.EMPTY
import static java.time.ZoneOffset.UTC

class IntervalTest extends Specification {

  static final ZonedDateTime START_OF_APRIL = ZonedDateTime.of(2020, 4, 1, 0, 0, 0, 0, UTC)
  static final ZonedDateTime END_OF_APRIL = ZonedDateTime.of(2020, 4, 30, 23, 59, 59, 0, UTC)
  static final ZonedDateTime START_OF_JUNE = ZonedDateTime.of(2020, 6, 1, 0, 0, 0, 0, UTC)
  static final ZonedDateTime END_OF_JUNE = ZonedDateTime.of(2020, 6, 30, 23, 59, 59, 0, UTC)

  def '(Non-)Empty intervals'() {
    given:
    Interval april = new Interval(START_OF_APRIL, END_OF_APRIL)
    Interval emptyApril = new Interval(END_OF_APRIL, START_OF_APRIL)

    expect:
    !april.isEmpty()
    emptyApril.isEmpty()
    EMPTY.isEmpty()
  }

  def 'Merging April with June and vice-versa yields empty interval'() {
    given:
    Interval april = new Interval(START_OF_APRIL, END_OF_APRIL)
    Interval june = new Interval(START_OF_JUNE, END_OF_JUNE)

    expect:
    april.mergedWith(june).isEmpty()
    june.mergedWith(april).isEmpty()
    EMPTY.mergedWith(april).isEmpty()
    april.mergedWith(EMPTY).isEmpty()
  }

  def 'Merging with NULL yields same instance'() {
    given:
    Interval april = new Interval(START_OF_APRIL, END_OF_APRIL)

    expect:
    april.mergedWith(null) == april
  }

  def 'Merging larger with narrower interval and vice-versa yields narrower interval'() {
    given:
    Interval aprilToJune = new Interval(START_OF_APRIL, END_OF_JUNE)
    Interval roughlyMay = new Interval(END_OF_APRIL, START_OF_JUNE)

    expect:
    aprilToJune.mergedWith(roughlyMay) == roughlyMay
    roughlyMay.mergedWith(aprilToJune) == roughlyMay
  }

}
