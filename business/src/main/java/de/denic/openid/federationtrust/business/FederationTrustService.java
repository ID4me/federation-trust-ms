/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business;

import com.nimbusds.jose.jwk.JWKSet;
import de.denic.openid.federationtrust.common.Entity;
import de.denic.openid.federationtrust.common.JWSOf;
import de.denic.openid.federationtrust.common.TrustAnchor;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfig;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfigClient;
import org.jgrapht.graph.AsUnmodifiableGraph;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import javax.annotation.concurrent.ThreadSafe;
import javax.inject.Singleton;
import java.net.URI;
import java.util.*;

import static de.denic.openid.federationtrust.common.TrustAnchor.NO_TRUST_ANCHORS;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toSet;

public interface FederationTrustService {

  String MDC_LEAF = "leaf";
  String MDC_PATH_LENGTH = "pathLength";
  String MDC_INITIAL_PATH_LENGTH_VALUE = "-";
  int ALLOWED_MAX_OF_MAX_PATH_LENGTH = 8;
  int DEFAULT_MAX_PATH_LENGTH = 4;

  @NonNull
  Builder seekForChainOf(@NonNull Entity leaf);

  @Immutable
  @Singleton
  final class Impl implements FederationTrustService {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    private static final Integer NO_PATH_LENGTH = null;

    private final FederationEntityConfigClient federationEntityConfigClient;
    private final FederationEntityStmtClient federationEntityStmtClient;

    public Impl(@NonNull final FederationEntityConfigClient federationEntityConfigClient,
                @NonNull final FederationEntityStmtClient federationEntityStmtClient) {
      this.federationEntityConfigClient = requireNonNull(federationEntityConfigClient, "Missing Federation Entity Config client component");
      this.federationEntityStmtClient = requireNonNull(federationEntityStmtClient, "Missing Federation Entity Statement client component");
    }

    @NonNull
    @Override
    public Builder seekForChainOf(@NonNull final Entity leaf) {
      return new BuilderImpl(leaf);
    }

    @NonNull
    private TrustChain seekFor(@NonNull final TrustChain trustChain) {
      final TrustChain.LeafNode leafNode = requireNonNull(trustChain, "Missing Trust Chain").getLeafNode();
      MDC.put(MDC_PATH_LENGTH, leafNode.getCurrentPathLengthAsString());
      final Collection<TrustChain.IntermediateNode> intermediateNodes = leafNode
              .provideNodesToFollowFrom(federationEntityConfigClient.getInfoFrom(leafNode.getEntity())
                      .orElse(null));
      followRecusively(intermediateNodes);
      return trustChain;
    }

    private void followRecusively(@Nullable final Collection<TrustChain.IntermediateNode> intermediateNodes) {
      if (intermediateNodes != null) {
        intermediateNodes.forEach(this::followRecusively);
      }
    }

    private void followRecusively(@NonNull final TrustChain.IntermediateNode intermediateNode) {
      MDC.put(MDC_PATH_LENGTH, requireNonNull(intermediateNode, "Missing intermediate node").getCurrentPathLengthAsString());
      final Entity issuer = intermediateNode.getEntity();
      final Optional<JWSOf<FederationEntityConfig>> optEntityConfigOfIntermediateNode = federationEntityConfigClient.getInfoFrom(issuer);
      final Optional<URI> optFederationAPIEndpointOfIntermediateNode = intermediateNode
              .provideFederationAPIEndpointFrom(optEntityConfigOfIntermediateNode
                      .orElse(null));
      if (optFederationAPIEndpointOfIntermediateNode.isEmpty()) {
        return;
      }

      final URI federationApiEndpointURIOfIntermediateNode = optFederationAPIEndpointOfIntermediateNode.get();
      final Entity subject = intermediateNode.getPreviousNode().getEntity();
      final JWSOf<FederationEntityStmt> entityStmtOfIntermediateNode = federationEntityStmtClient
              .getStatementFrom(issuer, federationApiEndpointURIOfIntermediateNode, subject)
              .orElse(null);
      final Collection<TrustChain.IntermediateNode> intermediateNodes = intermediateNode.provideNodesToFollowFrom(entityStmtOfIntermediateNode);
      followRecusively(intermediateNodes);
    }

    @NonNull
    private TrustGraph seekFor(@NonNull final Entity leaf,
                               @NonNull final Set<TrustAnchor> customTrustAnchors,
                               final int maxPathLength) {
      LOG.info("Seeking Trust Graph of '{}' (custom trust anchors {}, max path length: {})", leaf, customTrustAnchors, maxPathLength);
      final TrustChainContext trustChainContext = new TrustChainContext(leaf, customTrustAnchors, maxPathLength);
      trustChainContext.buildUpChainOfTrust();
      trustChainContext.verifyChainOfTrust();
      return new TrustGraph(trustChainContext);
    }

    @ThreadSafe
    public final class TrustChainContext {

      private final Object graphGuard = new Object();
      @GuardedBy("graphGuard")
      private final DirectedWeightedMultigraph<EntityVertex, FedEntityStmtEdge> graph = new DirectedWeightedMultigraph<>(FedEntityStmtEdge.class);
      private final Entity leaf;
      private final Set<TrustAnchor> customTrustAnchors;
      private final int maxPathLength;
      private volatile EntityVertex leafVertex;

      public TrustChainContext(@NonNull final Entity leaf,
                               @Nullable final Set<TrustAnchor> customTrustAnchors,
                               final int maxPathLength) {
        this.leaf = requireNonNull(leaf, "Missing leaf entity");
        this.maxPathLength = maxPathLength;
        this.customTrustAnchors = Set.copyOf(requireNonNullElse(customTrustAnchors, NO_TRUST_ANCHORS));
      }

      @NonNull
      public Entity getLeaf() {
        return leaf;
      }

      @NonNull
      public Optional<EntityVertex> getLeafVertex() {
        return ofNullable(leafVertex);
      }

      public int getMaxPathLength() {
        return maxPathLength;
      }

      @NonNull
      public Collection<TrustAnchor> getCustomTrustAnchors() {
        return customTrustAnchors;
      }

      @NonNull
      public AsUnmodifiableGraph<EntityVertex, FedEntityStmtEdge> getUnmodifiableGraph() {
        synchronized (graphGuard) {
          return new AsUnmodifiableGraph<>(graph);
        }
      }

      public boolean isCustomTrustAnchor(@Nullable final Entity entity) {
        return customTrustAnchors.stream()
                .anyMatch(trustAnchor -> trustAnchor.getEntity().equals(entity));
      }

      @NonNull
      public Optional<JWKSet> getJWKSetIfCustomTrustAnchor(@Nullable final Entity entity) {
        return customTrustAnchors.stream()
                .filter(trustAnchor -> trustAnchor.getEntity().equals(entity))
                .map(TrustAnchor::getJwkSet)
                .flatMap(Optional::stream)
                .findFirst();
      }

      public void buildUpChainOfTrust() {
        this.leafVertex = new EntityVertex(leaf, NO_PATH_LENGTH, this);
        if (!graphAdded(this.leafVertex)) {
          throw new RuntimeException("Internal error: Adding leaf entity to graph failed.");
        }

        MDC.put(MDC_PATH_LENGTH, leafVertex.getPathLengthAsString());
        try {
          this.leafVertex.loadYourConfigIfNotAlreadyDoneVia(federationEntityConfigClient);
          if (leafVertex.isValid()) {
            followRecursively(leafVertex);
          }
        } finally {
          MDC.remove(MDC_PATH_LENGTH);
        }
      }

      private void followRecursively(final EntityVertex entityVertex) {
        final Integer incrementedPathLength = entityVertex.getIncrementedPathLength();
        prepareSetOfNewOrKnownGraphVertices(entityVertex.getAuthorityHintsToFollow(), incrementedPathLength)
                .parallelStream()
                .filter(authorityHint -> !entityVertex.getEntity().equals(authorityHint.getEntity())) // Ignore self-referencing!
                .peek(authorityHintVertex -> MDC.put(MDC_PATH_LENGTH, authorityHintVertex.getPathLengthAsString()))
                .map(authorityHintVertex -> authorityHintVertex.loadYourConfigIfNotAlreadyDoneVia(federationEntityConfigClient))
                .filter(EntityVertex::notExceedsMaxPathLength)
                .filter(authorityHint -> unknownEdgeOf(entityVertex, authorityHint))
                .parallel()
                .peek(authorityHintVertex -> MDC.put(MDC_PATH_LENGTH, authorityHintVertex.getPathLengthAsString()))
                .peek(authHintVertex -> {
                  final FedEntityStmtEdge fedEntityStmtOfAuthHintRegardingEntity = authHintVertex.queryForFedEntityStmtEdgeRegarding(entityVertex, federationEntityStmtClient);
                  graphAddedEdge(entityVertex, authHintVertex, fedEntityStmtOfAuthHintRegardingEntity);
                })
                .filter(EntityVertex::isNoTrustAnchor)
                .filter(EntityVertex::notExceedsMaxPathLength)
                .forEach(this::followRecursively);
      }

      @NonNull
      private Set<EntityVertex> prepareSetOfNewOrKnownGraphVertices(@NonNull final Set<Entity> authorityHints,
                                                                    @NonNull final Integer incrementedPathLength) {
        return authorityHints.stream()
                .map(authorityHint -> new EntityVertex(authorityHint, incrementedPathLength, this))
                .map(this::addToGraphAsNewOrProvideKnown)
                .collect(toSet());
      }

      @NonNull
      private EntityVertex addToGraphAsNewOrProvideKnown(@NonNull final EntityVertex authorityHintVertex) {
        synchronized (graphGuard) {
          if (graphAdded(authorityHintVertex)) {
            return authorityHintVertex;
          }

          return replaceWithAlreadyKnownVertexFromGraph(authorityHintVertex);
        }
      }

      private boolean graphAdded(@NonNull final EntityVertex entityVertex) {
        synchronized (graphGuard) {
          return graph.addVertex(entityVertex);
        }
      }

      private EntityVertex replaceWithAlreadyKnownVertexFromGraph(@NonNull final EntityVertex entityVertex) {
        synchronized (graphGuard) {
          return graph.vertexSet().stream()
                  .filter(knownEntityVertex -> knownEntityVertex.getEntity().equals(entityVertex.getEntity()))
                  .findFirst()
                  .orElseThrow();
        }
      }

      private boolean unknownEdgeOf(@NonNull final EntityVertex sourceVertex,
                                    @NonNull final EntityVertex targetVertex) {
        synchronized (graphGuard) {
          return !graph.containsEdge(sourceVertex, targetVertex);
        }
      }

      private void graphAddedEdge(@NonNull final EntityVertex sourceVertex,
                                  @NonNull final EntityVertex targetVertex,
                                  @NonNull final FedEntityStmtEdge fedEntityStmtEdge) {
        synchronized (graphGuard) {
          graph.addEdge(sourceVertex, targetVertex, fedEntityStmtEdge);
        }
      }

      public void verifyChainOfTrust() {
        final Set<EntityVertex> entityVertices;
        final Set<FedEntityStmtEdge> federationEntityStmtEdges;
        synchronized (graphGuard) {
          entityVertices = graph.vertexSet();
          federationEntityStmtEdges = graph.edgeSet();
        }
        entityVertices.parallelStream().forEach(EntityVertex::verify);
        federationEntityStmtEdges.parallelStream().forEach(FedEntityStmtEdge::verify);
      }

      protected boolean exceedsMaxPathLength(@Nullable final Integer currentPathLength) {
        return (currentPathLength != null) && (currentPathLength > maxPathLength);
      }

    }

    @NotThreadSafe
    private final class BuilderImpl implements Builder {

      private final Entity leaf;
      private final Set<TrustAnchor> customTrustAnchors = new HashSet<>();
      private Integer maxPathLength;

      private BuilderImpl(@NonNull final Entity leaf) {
        this.leaf = requireNonNull(leaf, "Missing leaf Entity");
      }

      @NonNull
      @Override
      public Builder withCustomTrustAnchors(@Nullable final Collection<TrustAnchor> customTrustAnchors) {
        requireNonNullElse(customTrustAnchors, NO_TRUST_ANCHORS).stream()
                .filter(Objects::nonNull)
                .forEach(this.customTrustAnchors::add);
        return this;
      }

      @NonNull
      @Override
      public Builder withMaxPathLength(@Nullable final Integer maxPathLength) {
        if (maxPathLength != null && maxPathLength > ALLOWED_MAX_OF_MAX_PATH_LENGTH) {
          throw new IllegalArgumentException("Provided max path length '" + maxPathLength + "' exceeds allowed maximum value of '" + ALLOWED_MAX_OF_MAX_PATH_LENGTH + "'");
        }

        this.maxPathLength = maxPathLength;
        return this;
      }

      @NonNull
      @Override
      public TrustChain now() {
        LOG.info("Seeking Trust Chain of '{}' (custom trust anchors {}, max path length: {})", leaf, customTrustAnchors, maxPathLength);
        return seekFor(new TrustChain(leaf, customTrustAnchors, requireNonNullElse(maxPathLength, DEFAULT_MAX_PATH_LENGTH)));
      }

      @NonNull
      @Override
      public TrustGraph graphNow() {
        return seekFor(leaf, customTrustAnchors, requireNonNullElse(maxPathLength, DEFAULT_MAX_PATH_LENGTH));
      }

    }

  }

  interface Builder {

    @NonNull
    default Builder withCustomTrustAnchors(@Nullable TrustAnchor... customTrustAnchors) {
      return withCustomTrustAnchors(customTrustAnchors == null ? NO_TRUST_ANCHORS : asList(customTrustAnchors));
    }

    @NonNull
    Builder withCustomTrustAnchors(@Nullable Collection<TrustAnchor> trustAnchors);

    /**
     * @param maxPathLength If missing default of {@link #DEFAULT_MAX_PATH_LENGTH} is applied. Max allowed value is {@link #ALLOWED_MAX_OF_MAX_PATH_LENGTH}
     * @return <code>this</code> instance
     */
    @NonNull
    Builder withMaxPathLength(@Nullable Integer maxPathLength);

    @NonNull
    TrustChain now();

    @NonNull
    TrustGraph graphNow();

  }

}
