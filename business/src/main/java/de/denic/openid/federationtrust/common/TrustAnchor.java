/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.common;

import com.nimbusds.jose.jwk.JWKSet;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.text.ParseException;
import java.util.Collection;
import java.util.Optional;
import java.util.regex.Pattern;

import static io.micronaut.core.util.StringUtils.isEmpty;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Base64.Decoder;
import static java.util.Base64.getUrlDecoder;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

@Immutable
public class TrustAnchor {

  public static final Collection<TrustAnchor> NO_TRUST_ANCHORS = emptyList();

  private static final Decoder BASE64_URL_DECODER = getUrlDecoder();
  private static final Pattern HORIZONTAL_WHITE_SPACES = Pattern.compile("\\h+");

  private final Entity entity;
  private final JWKSet jwkSet;

  public TrustAnchor(@NonNull final Entity entity,
                     @Nullable final JWKSet jwkSet) {
    this.entity = requireNonNull(entity, "Missing trust anchor entity");
    this.jwkSet = jwkSet;
  }

  @NonNull
  public Entity getEntity() {
    return entity;
  }

  @NonNull
  public Optional<JWKSet> getJwkSet() {
    return ofNullable(jwkSet);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final TrustAnchor that = (TrustAnchor) o;

    if (!entity.equals(that.entity))
      return false;
    return jwkSet != null ? jwkSet.equals(that.jwkSet) : that.jwkSet == null;
  }

  @Override
  public int hashCode() {
    int result = entity.hashCode();
    result = 31 * result + (jwkSet != null ? jwkSet.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "{" + entity +
            (jwkSet == null ? "" : ", jwkSet=" + jwkSet.toJSONObject().toJSONString()) +
            '}';
  }

  /**
   * @param jwkSetValue If starting with <code>{</code> interpreted as plain JSON; otherwise base64-encoded JSON.
   */
  @Nullable
  public static TrustAnchor fromEntityAndJWKSet(@Nullable final String entityValue,
                                                @Nullable final String jwkSetValue) {
    if (isEmpty(entityValue)) {
      return null;
    }

    final Entity entity = Entity.ofHost(entityValue);
    return fromEntityAndJWKSet(entity, jwkSetValue);
  }

  /**
   * @see #fromEntityAndJWKSet(String, String)
   */
  @Nullable
  public static TrustAnchor fromEntityAndJWKSet(@Nullable final Entity entity,
                                                @Nullable final String jwkSetValue) {
    if (entity == null) {
      return null;
    }

    final String strippedJWKSetValue = (jwkSetValue == null ? null : jwkSetValue.strip());
    final String jwkSetJSON;
    if (isEmpty(strippedJWKSetValue)) {
      jwkSetJSON = null;
    } else if (strippedJWKSetValue.startsWith("{")) {
      jwkSetJSON = strippedJWKSetValue;
    } else {
      final String assumedBase64EncodedJWKSet = HORIZONTAL_WHITE_SPACES.matcher(strippedJWKSetValue).replaceAll("");
      jwkSetJSON = new String(BASE64_URL_DECODER.decode(assumedBase64EncodedJWKSet), UTF_8);
    }
    final JWKSet jwkSet;
    try {
      jwkSet = (isEmpty(jwkSetJSON) ? null : JWKSet.parse(jwkSetJSON));
    } catch (final ParseException e) {
      throw new IllegalArgumentException("Failed to parse JWKSet from: '" + jwkSetJSON + "'", e);
    }

    return new TrustAnchor(entity, jwkSet);
  }

  @Nullable
  public static TrustAnchor fromEntity(@Nullable final Entity entity) {
    return fromEntityAndJWKSet(entity, null);
  }

}
