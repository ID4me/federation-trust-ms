/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.common;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static de.denic.openid.federationtrust.common.CommonFederationEntityStmt.UTC;
import static java.time.Instant.ofEpochSecond;
import static java.time.ZonedDateTime.ofInstant;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.reverseOrder;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Immutable
public final class Interval {

  public static final Interval EMPTY = new Interval(ofInstant(ofEpochSecond(1L), UTC), ofInstant(ofEpochSecond(0L), UTC));

  private final ZonedDateTime startsAt, endsAt;
  private final boolean isEmpty;

  public Interval(@NonNull final ZonedDateTime startsAt,
                  @NonNull final ZonedDateTime endsAt) {
    this.startsAt = requireNonNull(startsAt, "Missing starting temporal");
    this.endsAt = requireNonNull(endsAt, "Missing ending temporal");
    this.isEmpty = endsAt.isBefore(startsAt);
  }

  public boolean contains(@Nullable final ZonedDateTime timestamp) {
    return (timestamp != null)
            && (timestamp.isAfter(startsAt)
            && timestamp.isBefore(endsAt));
  }

  public boolean isEmpty() {
    return isEmpty;
  }

  public boolean notEmpty() {
    return !isEmpty();
  }

  @NonNull
  public Interval mergedWith(@Nullable final Interval other) {
    return (other == null || this.isEmpty() ? this
            : (other.isEmpty() ? other
            : new Interval(latestOf(this.startsAt, other.startsAt), earliestOf(this.endsAt, other.endsAt))));
  }

  /**
   * @return {@link Optional#empty()} if {@link #isEmpty()}.
   */
  @NonNull
  public Optional<ZonedDateTime> getStart() {
    return (isEmpty() ? empty() : of(startsAt));
  }

  /**
   * @return {@link Optional#empty()} if {@link #isEmpty()}.
   */
  @NonNull
  public Optional<ZonedDateTime> getEnd() {
    return (isEmpty() ? empty() : of(endsAt));
  }

  @NonNull
  public static ZonedDateTime latestOf(@NonNull final ZonedDateTime... timestamps) {
    return getMaxAccordingTo(naturalOrder(), timestamps);
  }

  @NonNull
  public static ZonedDateTime earliestOf(@NonNull final ZonedDateTime... timestamps) {
    return getMaxAccordingTo(reverseOrder(), timestamps);
  }

  @NonNull
  private static ZonedDateTime getMaxAccordingTo(@NonNull final Comparator<ZonedDateTime> comparator,
                                                 @NonNull final ZonedDateTime[] timestamps) {
    return List.of(requireNonNull(timestamps, "Missing timestamps"))
            .stream()
            .max(requireNonNull(comparator, "Missing comparator"))
            .orElseThrow();
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final Interval interval = (Interval) o;

    if (!startsAt.equals(interval.startsAt))
      return false;
    return endsAt.equals(interval.endsAt);
  }

  @Override
  public int hashCode() {
    int result = startsAt.hashCode();
    result = 31 * result + endsAt.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return (isEmpty ? "<EMPTY>"
            : "[" + startsAt + ',' + endsAt + ']');
  }

  public static int endComparator(@NonNull final Interval first, @NonNull final Interval second) {
    return requireNonNull(first, "Missing first interval").endsAt
            .compareTo(requireNonNull(second, "Missing second interval").endsAt);
  }

}
