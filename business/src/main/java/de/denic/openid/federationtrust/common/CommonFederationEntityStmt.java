/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.common;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.JsonNode;
import com.nimbusds.jose.jwk.JWKSet;
import net.minidev.json.JSONObject;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.text.ParseException;
import java.time.ZonedDateTime;

import static java.time.Instant.ofEpochSecond;
import static java.util.Objects.requireNonNull;

@JsonPropertyOrder({"iss", "sub", "iat", "exp", "jwks", "authority-hints"})
public interface CommonFederationEntityStmt extends JWKSetProvider, ValidityConstrained {

  @JsonGetter("iss")
  @NonNull
  Entity getIssuer();

  @JsonGetter("sub")
  @NonNull
  Entity getSubject();

  @JsonIgnore
  default boolean isSelfSigned() {
    return getIssuer().equals(getSubject());
  }

  @NonNull
  <PTYPE extends JWKSetProvider> JWSOf<PTYPE> sign(@NonNull PTYPE toSign);

  @Immutable
  @JsonIgnoreProperties(ignoreUnknown = true)
  abstract class Impl implements CommonFederationEntityStmt {

    private final Entity issuer, subject;
    private final Interval validityInterval;
    private final JWKSet jwkSetOfSubject;

    public Impl(@NonNull final URI issuer,
                @NonNull final URI subject,
                @NonNull final Long issuedAt,
                @NonNull final Long expiresAt,
                @Nullable final JWKSet jwkSetOfSubject) {
      this.issuer = Entity.ofURI(requireNonNull(issuer, "Missing issuer ('iss')"));
      this.subject = Entity.ofURI(requireNonNull(subject, "Missing subject ('sub')"));
      this.validityInterval = new Interval(ofEpochSecond(requireNonNull(issuedAt, "Missing issuing timestamp ('iat')")).atZone(UTC),
              ofEpochSecond(requireNonNull(expiresAt, "Missing expiration timestamp ('exp')")).atZone(UTC));
      this.jwkSetOfSubject = (jwkSetOfSubject == null ? new JWKSet() : jwkSetOfSubject);
    }

    @NonNull
    protected static JWKSet parseToJWKSet(@NonNull final JsonNode jsonNode) {
      try {
        return JWKSet.parse(requireNonNull(jsonNode, "Missing JSON node representing JWK-set").toString());
      } catch (final ParseException e) {
        throw new IllegalArgumentException("Parsing value of 'jwks' element failed", e);
      }
    }

    @NonNull
    @Override
    public final Entity getIssuer() {
      return issuer;
    }

    @NonNull
    @Override
    public final Entity getSubject() {
      return subject;
    }

    @JsonIgnore
    @NonNull
    @Override
    public final JWKSet getPublicJWKSetOfSubject() {
      return jwkSetOfSubject.toPublicJWKSet();
    }

    @JsonGetter("jwks")
    @NonNull
    public final JSONObject getPublicJWKSetOfSubjectAsJSON() {
      return getPublicJWKSetOfSubject().toJSONObject();
    }

    @NonNull
    @Override
    public <PTYPE extends JWKSetProvider> JWSOf<PTYPE> sign(@NonNull final PTYPE toSign) {
      return JWSOf.signed(toSign, this.jwkSetOfSubject.getKeys().get(0));
    }

    @JsonIgnore
    @NonNull
    @Override
    public Interval getValidityInterval() {
      return validityInterval;
    }

    @JsonGetter("iat")
    @Nullable
    public Long getIssuedAt() {
      return validityInterval.getStart()
              .map(ZonedDateTime::toEpochSecond)
              .orElse(null);
    }

    @JsonGetter("exp")
    @Nullable
    public Long getExpiresAt() {
      return validityInterval.getEnd()
              .map(ZonedDateTime::toEpochSecond)
              .orElse(null);
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final Impl impl = (Impl) o;

      if (!issuer.equals(impl.issuer))
        return false;
      if (!subject.equals(impl.subject))
        return false;
      if (!validityInterval.equals(impl.validityInterval))
        return false;
      return jwkSetOfSubject.toJSONObject().equals(impl.jwkSetOfSubject.toJSONObject());
    }

    @Override
    public int hashCode() {
      int result = issuer.hashCode();
      result = 31 * result + subject.hashCode();
      result = 31 * result + validityInterval.hashCode();
      result = 31 * result + jwkSetOfSubject.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "issuer='" + issuer +
              "', subject='" + subject +
              "', validity=" + validityInterval +
              ", jwkSetOfSubject=" + jwkSetOfSubject;
    }

  }

}
