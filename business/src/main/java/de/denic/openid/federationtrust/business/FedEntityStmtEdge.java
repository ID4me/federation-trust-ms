/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business;

import com.nimbusds.jose.jwk.JWKSet;
import de.denic.openid.federationtrust.common.*;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfig;
import org.jgrapht.graph.DefaultWeightedEdge;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static de.denic.openid.federationtrust.business.FederationTrustService.ALLOWED_MAX_OF_MAX_PATH_LENGTH;
import static de.denic.openid.federationtrust.common.Interval.EMPTY;
import static de.denic.openid.federationtrust.common.TenthOfSecond.TENTH_OF_SECOND;
import static java.util.Collections.synchronizedList;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

@ThreadSafe
public final class FedEntityStmtEdge extends DefaultWeightedEdge implements OperationTimer, ValidityConstrained, Comparable<FedEntityStmtEdge> {

  public static final double VALID_PATH_WEIGHT = 1.0d;
  public static final double INVALID_PATH_WEIGHT = 100.0d * ALLOWED_MAX_OF_MAX_PATH_LENGTH * VALID_PATH_WEIGHT;

  private final OperationTimer.Impl operationTimer = new OperationTimer.Impl(TENTH_OF_SECOND);
  private final EntityVertex issuerVertex, subjectVertex;
  private final URI optIssuersFederationApiEndpointURI;
  private final int preCalculatedHashCode;
  private final List<String> issues = synchronizedList(new LinkedList<>());
  private final List<String> messages = synchronizedList(new LinkedList<>());
  private volatile JWSOf<FederationEntityStmt> jwsOfFederationEntityStmt;
  private volatile boolean stillValid;

  public FedEntityStmtEdge(@NonNull final EntityVertex issuerVertex,
                           @Nullable final URI issuersFederationApiEndpointURI,
                           @NonNull final EntityVertex subjectVertex) {
    this.issuerVertex = requireNonNull(issuerVertex, "Missing issuer");
    this.subjectVertex = requireNonNull(subjectVertex, "Missing subject");
    this.optIssuersFederationApiEndpointURI = issuersFederationApiEndpointURI;
    this.preCalculatedHashCode = precalculateHashCodeFrom(this.issuerVertex, this.subjectVertex);
  }

  private static int precalculateHashCodeFrom(@NonNull final EntityVertex issuer,
                                              @NonNull final EntityVertex subject) {
    int result = issuer.getEntity().hashCode();
    result = 31 * result + subject.getEntity().hashCode();
    return result;
  }

  @Override
  protected double getWeight() {
    return (isValid() && issuerVertex.isValid() ? VALID_PATH_WEIGHT : INVALID_PATH_WEIGHT);
  }

  public boolean isValid() {
    return stillValid;
  }

  @NonNull
  public Entity getIssuerEntity() {
    return issuerVertex.getEntity();
  }

  @NonNull
  public Entity getSubjectEntity() {
    return subjectVertex.getEntity();
  }

  public boolean isLinking(@Nullable final Entity subject, @Nullable final Entity issuer) {
    return (getIssuerEntity().equals(issuer)) && (getSubjectEntity().equals(subject));
  }

  @NonNull
  public List<String> getIssues() {
    return unmodifiableList(issues);
  }

  @NonNull
  public List<String> getMessages() {
    return unmodifiableList(messages);
  }

  @NonNull
  public FedEntityStmtEdge loadYourFederationEntityStmtApplying(@NonNull final FederationEntityStmtClient federationEntityStmtClient) {
    final Entity issuer = issuerVertex.getEntity();
    if (optIssuersFederationApiEndpointURI == null) {
      return addIssueRegardingIssuer("Missing Federation API Endpoint.");
    }

    requireNonNull(federationEntityStmtClient, "Missing Federation Entity Stmt client");
    final Entity subject = subjectVertex.getEntity();
    final Optional<JWSOf<FederationEntityStmt>> optJWSOfFedEntityStmt = operationTimer.timed(() -> federationEntityStmtClient
            .getStatementFrom(issuer, optIssuersFederationApiEndpointURI, subject));
    if (optJWSOfFedEntityStmt.isEmpty()) {
      return addIssueRegardingIssuer("Missing Federation Entity Stmt regarding subject '" + subject + "'.");
    }

    this.jwsOfFederationEntityStmt = optJWSOfFedEntityStmt.get();
    final FederationEntityStmt fedEntityStmt = this.jwsOfFederationEntityStmt.getPayload();
    if (!fedEntityStmt.isValidNow()) {
      return addIssueRegardingIssuer("Federation Entity Stmt currently invalid: " + fedEntityStmt.getValidityInterval() + ".");
    }

    if (fedEntityStmt.isSelfSigned()) {
      return addIssueRegardingIssuer("Federation Entity Stmt IS self-signed.");
    }

    final Entity issuerOfEntityStmt = fedEntityStmt.getIssuer();
    if (!issuer.equals(issuerOfEntityStmt)) {
      return addIssueRegardingIssuer("Mismatching issuer: '" + issuer + "' <> '" + issuerOfEntityStmt + "' (from Federation Entity Stmt).");
    }

    final Entity subjectOfEntityStmt = fedEntityStmt.getSubject();
    if (!subject.equals(subjectOfEntityStmt)) {
      return addIssueRegardingIssuer("Mismatching subject: '" + subject + "' <> '" + subjectOfEntityStmt + "' (from Federation Entity Stmt).");
    }

    stillValid = true;
    return this;
  }

  @NonNull
  public Optional<JWSOf<FederationEntityStmt>> getJWSOfFederationEntityStmt() {
    return ofNullable(jwsOfFederationEntityStmt);
  }

  @NonNull
  private FedEntityStmtEdge addIssueRegardingIssuer(@NonNull final String issue) {
    return addIssueRegarding(issuerVertex, issue);
  }

  private void addIssueRegardingSubject(@NonNull final String issue) {
    addIssueRegarding(subjectVertex, issue);
  }

  @NonNull
  private FedEntityStmtEdge addIssueRegarding(@NonNull final EntityVertex target, @NonNull final String issue) {
    issues.add(requireNonNull(target, "Missing target").getEntity() + ": " + requireNonNull(issue, "Missing issue"));
    stillValid = false;
    return this;
  }

  private void addMessageRegardingSubject(@NonNull final String message) {
    messages.add(subjectVertex.getEntity() + ": " + requireNonNull(message, "Missing message"));
  }

  @NonNull
  public Interval getOperationInterval() {
    return operationTimer.getOperationInterval();
  }

  @NonNull
  @Override
  public Interval getValidityInterval() {
    return getJWSOfFederationEntityStmt()
            .map(JWSOf::getPayload)
            .map(FederationEntityStmt::getValidityInterval)
            .orElse(EMPTY);
  }

  public void verify() {
    if (!issuerVertex.isValid()) {
      stillValid = false;
      return;
    }

    final JWKSet issuersJWKSetOfSubject = getJWSOfFederationEntityStmt()
            .map(JWSOf::getPayload)
            .map(FederationEntityStmt::getPublicJWKSetOfSubject)
            .orElse(null);
    final Optional<JWSOf<FederationEntityConfig>> optJWSOfFederationEntityConfig = this.subjectVertex
            .getJWSOfFederationEntityConfig();
    if (optJWSOfFederationEntityConfig.isEmpty()) {
      addIssueRegardingSubject("Missing JWS of Federation Entity Configuration.");
    } else {
      try {
        final String keyIDVerifyingSubjectsFederationEntityConfig = optJWSOfFederationEntityConfig.get()
                .getVerifyingKeyIDFrom(issuersJWKSetOfSubject);
        addMessageRegardingSubject("Federation Entity Configuration verified with JWK (ID '" + keyIDVerifyingSubjectsFederationEntityConfig + "') of authority '" + issuerVertex.getEntity() + "'.");
      } catch (JWSVerificationException e) {
        addIssueRegardingSubject("FAILED verification of Federation Entity Configuration with JWK set of authority '" + issuerVertex.getEntity() + "'.");
      }
    }
  }

  @Override
  public int compareTo(final FedEntityStmtEdge other) {
    final int comparedIssuers = this.issuerVertex.compareTo(other.issuerVertex);
    if (comparedIssuers != 0) {
      return comparedIssuers;
    }

    return this.subjectVertex.compareTo(other.subjectVertex);
  }

  /**
   * HINT: It's <strong>very</strong> important for <a href="https://jgrapht.org/guide/VertexAndEdgeTypes">JGraphT library</a> edges
   * to implement {@link #equals(Object)}/{@link #hashCode()} using identifying fields <strong>ONLY</strong>!
   */
  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final FedEntityStmtEdge edge = (FedEntityStmtEdge) o;

    final Entity issuer = issuerVertex.getEntity();
    if (!issuer.equals(edge.issuerVertex.getEntity()))
      return false;
    final Entity subject = subjectVertex.getEntity();
    return subject.equals(edge.subjectVertex.getEntity());
  }

  /**
   * @see #equals(Object)
   */
  @Override
  public int hashCode() {
    return preCalculatedHashCode;
  }

  @Override
  public String toString() {
    return "{" + (stillValid ? "VALID" : "INVALID") +
            " '" + issuerVertex.getEntity() + "'" +
            " => '" + subjectVertex.getEntity() + "'" +
            ", evaluated within " + operationTimer + '}';
  }

}
