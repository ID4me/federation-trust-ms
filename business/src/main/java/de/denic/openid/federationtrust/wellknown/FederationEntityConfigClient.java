/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.wellknown;

import de.denic.openid.federationtrust.common.Entity;
import de.denic.openid.federationtrust.common.JWSOf;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.exceptions.HttpClientException;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.http.uri.UriBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.core.annotation.NonNull;
import javax.annotation.concurrent.Immutable;
import javax.inject.Singleton;
import java.net.URI;
import java.util.Optional;

import static de.denic.openid.federationtrust.util.LogUtil.shortenBodyOf;
import static io.micronaut.http.HttpRequest.GET;
import static io.micronaut.http.HttpStatus.NOT_FOUND;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public interface FederationEntityConfigClient {

  @NonNull
  Optional<JWSOf<FederationEntityConfig>> getInfoFrom(@NonNull final Entity entity);

  @Immutable
  @Singleton
  final class Impl implements FederationEntityConfigClient {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    private static final String WELL_KNOWN_OPENID_FEDERATION = "/.well-known/openid-federation";

    private final HttpClient httpClient;

    public Impl(@NonNull final HttpClient httpClient) {
      this.httpClient = requireNonNull(httpClient, "Missing HTTP client component");
    }

    @Override
    @NonNull
    public Optional<JWSOf<FederationEntityConfig>> getInfoFrom(@NonNull final Entity entity) {
      final URI uriOfEntity = requireNonNull(entity, "Missing Entity").asHttpsURI();
      final URI rfc8615CompliantWellKnownURI = UriBuilder.of(uriOfEntity)
              .replacePath(WELL_KNOWN_OPENID_FEDERATION)
              .path(uriOfEntity.getPath())
              .build();
      try {
        return getInfoQuerying(rfc8615CompliantWellKnownURI, true, "Q");
      } catch (final HttpClientResponseException notFoundException) {
        final URI recommendedRFC8615IncompliantWellKnownURI = UriBuilder.of(uriOfEntity)
                .path(WELL_KNOWN_OPENID_FEDERATION)
                .build();
        if (rfc8615CompliantWellKnownURI.equals(recommendedRFC8615IncompliantWellKnownURI)) {
          return empty();
        }

        return getInfoQuerying(recommendedRFC8615IncompliantWellKnownURI, false, "FALLBACK q");
      }
    }

    /**
     * @throws HttpClientResponseException <strong>only</strong> with <code>404</code> responses <strong>AND</strong> if param {@code throwNotFoundException} is set!
     */
    @NonNull
    private Optional<JWSOf<FederationEntityConfig>> getInfoQuerying(@NonNull final URI wellKnownURI,
                                                                    final boolean throwNotFoundException,
                                                                    final String logMessagePrefix) throws HttpClientResponseException {
      try {
        final String responseBody = httpClient.toBlocking()
                .retrieve(GET(requireNonNull(wellKnownURI, "Missing well-known URI")));
        final JWSOf<FederationEntityConfig> jwsOfEntityConfig = JWSOf.from(responseBody, FederationEntityConfig.class);
        LOG.info("{}ueried '{}': {}", logMessagePrefix, wellKnownURI, jwsOfEntityConfig);
        return of(jwsOfEntityConfig);
      } catch (final HttpClientResponseException e) {
        final HttpStatus httpStatus = e.getStatus();
        LOG.info("{}uerying '{}' failed. Response code '{} {}', body: '{}'",
                logMessagePrefix, wellKnownURI, httpStatus.getCode(), httpStatus.getReason(), shortenBodyOf(e.getResponse()));
        if (throwNotFoundException && NOT_FOUND.equals(httpStatus)) {
          throw e;
        }
      } catch (final HttpClientException e) {
        LOG.info("{}uerying '{}' failed: {} ({})", logMessagePrefix, wellKnownURI, e.getMessage(), e.getClass().getName());
      } catch (final RuntimeException e) {
        LOG.warn("{}uerying '{}' failed.", logMessagePrefix, wellKnownURI, e);
      }
      return empty();
    }

  }

}
