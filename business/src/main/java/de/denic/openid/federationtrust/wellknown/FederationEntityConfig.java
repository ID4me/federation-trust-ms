/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.wellknown;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nimbusds.jose.jwk.JWKSet;
import de.denic.openid.federationtrust.common.CommonFederationEntityStmt;
import de.denic.openid.federationtrust.common.Entity;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.*;

import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableSet;
import static java.util.Objects.requireNonNullElse;
import static java.util.Optional.ofNullable;

@JsonSerialize(as = FederationEntityConfig.Impl.class)
@JsonDeserialize(as = FederationEntityConfig.Impl.class)
public interface FederationEntityConfig extends CommonFederationEntityStmt {

  /**
   * @return Usually immutable!
   */
  @NonNull
  Set<Entity> getAuthorityHints();

  /**
   * If no Authority Hints are given (see <a href="https://openid.net/specs/openid-connect-federation-1_0.html#entity-statement">chapter 2.1 of specification</a>).
   *
   * @see #getAuthorityHints()
   */
  @JsonIgnore
  default boolean isTrustAnchor() {
    return getAuthorityHints().isEmpty();
  }

  @NonNull
  Optional<URI> getFederationApiEndpoint();

  @NonNull
  Optional<Integer> getMaxPathLength();

  @Immutable
  @JsonIgnoreProperties(ignoreUnknown = true)
  final class Impl extends CommonFederationEntityStmt.Impl implements FederationEntityConfig {

    public static final Collection<URI> NO_AUTHORITY_HINTS = emptySet();

    private static final JsonPointer FEDERATION_API_ENDPOINT_JSON_PATH = JsonPointer.compile("/federation_entity/federation_api_endpoint");
    private static final JsonNodeFactory JSON_NODE_FACTORY = JsonNodeFactory.instance;

    private final URI federationApiEndpoint;
    private final Set<Entity> authorityHints = new HashSet<>();
    private final Integer maxPathLength;

    public Impl(@NonNull final URI issuer,
                @NonNull final URI subject,
                @NonNull final Long issuedAt,
                @NonNull final Long expiresAt,
                @Nullable final JWKSet jwkSetOfSubject,
                @Nullable final JsonNode metaDataJsonNode,
                @Nullable final JsonNode constraintsJsonNode,
                @Nullable final Collection<URI> authorityHints) {
      super(issuer, subject, issuedAt, expiresAt, jwkSetOfSubject);
      this.federationApiEndpoint = (metaDataJsonNode == null ? null : extractFederationApiEndpointFrom(metaDataJsonNode));
      this.maxPathLength = (constraintsJsonNode == null ? null : extractMaxPathLengthFrom(constraintsJsonNode));
      requireNonNullElse(authorityHints, NO_AUTHORITY_HINTS).forEach(authorityHint -> this.authorityHints.add(Entity.ofURI(authorityHint)));
    }

    public Impl(@NonNull @JsonProperty("iss") final URI issuer,
                @NonNull @JsonProperty("sub") final URI subject,
                @NonNull @JsonProperty("iat") final Long issuedAt,
                @NonNull @JsonProperty("exp") final Long expiresAt,
                @NonNull @JsonProperty("jwks") final JsonNode jwksJsonValue,
                @Nullable @JsonProperty("metadata") final JsonNode metaDataJsonNode,
                @Nullable @JsonProperty("constraints") final JsonNode constraintsJsonNode,
                @Nullable @JsonProperty("authority_hints") final Collection<URI> authorityHints) {
      this(issuer, subject, issuedAt, expiresAt, parseToJWKSet(jwksJsonValue), metaDataJsonNode, constraintsJsonNode, authorityHints);
    }

    @Nullable
    private Integer extractMaxPathLengthFrom(final JsonNode constraintsJsonNode) {
      final JsonNode maxPathLengthJsonNode = constraintsJsonNode.get("max_path_length");
      return (maxPathLengthJsonNode == null ? null : maxPathLengthJsonNode.asInt());
    }

    @Nullable
    private URI extractFederationApiEndpointFrom(@NonNull final JsonNode metaDataJsonNode) {
      final JsonNode federationApiEndpointJsonNode = metaDataJsonNode.at(FEDERATION_API_ENDPOINT_JSON_PATH);
      return (federationApiEndpointJsonNode.isMissingNode() ? null : URI.create(federationApiEndpointJsonNode.asText()));
    }

    @JsonIgnore
    @NonNull
    @Override
    public Optional<Integer> getMaxPathLength() {
      return ofNullable(maxPathLength);
    }

    @JsonIgnore
    @NonNull
    @Override
    public Optional<URI> getFederationApiEndpoint() {
      return ofNullable(federationApiEndpoint);
    }

    @JsonGetter("metadata")
    @Nullable
    public ObjectNode getMetaDataObjectNode() {
      if (federationApiEndpoint == null) {
        return null;
      }

      final ObjectNode metaDataNode = JSON_NODE_FACTORY.objectNode();
      metaDataNode.putObject("federation_entity")
              .put("federation_api_endpoint", federationApiEndpoint.toASCIIString());
      return metaDataNode;
    }

    @JsonGetter("authority_hints")
    @NonNull
    @Override
    public Set<Entity> getAuthorityHints() {
      return unmodifiableSet(authorityHints);
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;
      if (!super.equals(o))
        return false;

      final FederationEntityConfig.Impl impl = (FederationEntityConfig.Impl) o;

      if (federationApiEndpoint != null ? !federationApiEndpoint.equals(impl.federationApiEndpoint) : impl.federationApiEndpoint != null)
        return false;
      if (!authorityHints.equals(impl.authorityHints))
        return false;
      return maxPathLength != null ? maxPathLength.equals(impl.maxPathLength) : impl.maxPathLength == null;
    }

    @Override
    public int hashCode() {
      int result = super.hashCode();
      result = 31 * result + (federationApiEndpoint != null ? federationApiEndpoint.hashCode() : 0);
      result = 31 * result + authorityHints.hashCode();
      result = 31 * result + (maxPathLength != null ? maxPathLength.hashCode() : 0);
      return result;
    }

    @Override
    public String toString() {
      return "{" + super.toString() +
              ", federationApiEndpoint=" + (federationApiEndpoint == null ? "<NULL>" : "'" + federationApiEndpoint + "'") +
              ", authorityHints=" + authorityHints +
              "', maxPathLength=" + (Objects.toString(maxPathLength, "<NULL>")) +
              '}';
    }

  }

}
