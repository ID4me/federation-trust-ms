/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.common;

import com.fasterxml.jackson.annotation.JsonValue;
import io.micronaut.http.exceptions.UriSyntaxException;
import io.micronaut.http.uri.UriBuilder;

import io.micronaut.core.annotation.NonNull;
import javax.annotation.concurrent.Immutable;
import java.net.URI;

import static java.util.Objects.requireNonNull;

/**
 * Represents an <a href="https://openid.net/specs/openid-connect-federation-1_0.html#rfc.section.1.2">Entity (Identifier)</a>, usually something like
 * <ul>
 *   <li>a host name,</li>
 *   <li>an {@link URI}.</li>
 * </ul>
 */
@Immutable
public final class Entity implements Comparable<Entity> {

  private final String plainValue;
  private final URI httpsURI;
  private final int preCalculatedHashCode;

  private Entity(@NonNull final String value,
                 @NonNull final URI httpsURI) {
    this.plainValue = requireNonNull(value, "Missing value");
    this.httpsURI = requireNonNull(httpsURI, "Missing URI");
    this.preCalculatedHashCode = this.httpsURI.hashCode();
  }

  @JsonValue
  @NonNull
  public String getPlainValue() {
    return plainValue;
  }

  @NonNull
  public URI asHttpsURI() {
    return httpsURI;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final Entity entityID = (Entity) o;

    return httpsURI.equals(entityID.httpsURI);
  }

  @Override
  public int compareTo(final Entity o) {
    return httpsURI.compareTo(o.httpsURI);
  }

  @Override
  public int hashCode() {
    return preCalculatedHashCode;
  }

  @Override
  public String toString() {
    return plainValue;
  }

  @NonNull
  public static Entity ofHost(@NonNull final String host) throws IllegalArgumentException {
    return new Entity(requireNonNull(host, "Missing host"),
            URI.create("https://" + host));
  }

  @NonNull
  public static Entity ofURI(@NonNull final URI uri) throws UriSyntaxException {
    return new Entity(requireNonNull(uri, "Missing uri").toString(),
            UriBuilder.of(uri).scheme("https").build());
  }

}
