/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.ui.view;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextArea;

import de.denic.openid.federationtrust.business.EntityVertex;
import de.denic.openid.federationtrust.business.TrustGraph;
import de.denic.openid.federationtrust.ui.common.Size;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;

import io.micronaut.core.annotation.NonNull;

/**
 * The form view contains requires a click listener.
 */
@CssImport("./styles/shared-styles.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
public class DetailsView extends HorizontalLayout {

  private static final long serialVersionUID = -7135613105898983977L;
  // Layouts for status and entity select view
  private final StatusView statusView;
  private EntityView entityView;
  private final Size size;
  private final TrustGraph trustGraph;


  public DetailsView(@NonNull final Size size, @NonNull final TrustGraph trustGraph) {

    this.trustGraph = requireNonNull(trustGraph, "Missing TrustGraph Object");
    this.size = requireNonNull(size, "Missing Size Object");
    statusView = new StatusView(new Size(size.getWidth()/2, size.getHeight(), size.getUnitType()), trustGraphToStatus());
    
    setWidth(this.size.getWidthSize());
    setMaxWidth(this.size.getWidthSize());
    setMaxHeight(this.size.getHeightSize());
    this.setJustifyContentMode(FlexComponent.JustifyContentMode.START);
    this.setSpacing(false);
    
    add(statusView);
    addEmptyEntityView();
  }

  public StatusView getStatusView() {
    return statusView;
  }

  public class StatusView extends FormLayout {

    private static final long serialVersionUID = 4786834355890457191L;
    private final Size size;

    public StatusView(@NonNull final Size size, @NonNull final ArrayList<String> verificationStatus) {

      this.size = requireNonNull(size, "Missing Size Object");

      requireNonNull(verificationStatus, "Missing Status from Verification");

      for (String item : verificationStatus) {
        String[] split = item.split(";");
        addFormItem(new Text(split[1]), split[0]);
      }

      setWidth(this.size.getWidthSize());
      //setHeight(height);
      setMinWidth(this.size.getWidthSize());
      setMaxWidth(this.size.getWidthSize());
      setMaxHeight(this.size.getHeightSize());
    }
  }

  public void addEntity(@NonNull final EntityVertex vertex) {
    requireNonNull(vertex, "Missing Vertex Componnent");
    clearEntityView();
    entityView = new EntityView(new Size(size.getWidth()/2, size.getHeight(), size.getUnitType()), vertex);
    add(entityView);
  }

  private void clearEntityView() {
    if (entityView != null){remove(entityView);};
  }

  public void addEmptyEntityView() {
   clearEntityView();
   entityView = new EntityView(new Size(size.getWidth()/2, size.getHeight(), size.getUnitType()), null);
   add(entityView);
  }

  public EntityView getEntityView() {
    return entityView;
  }

  public class EntityView extends FormLayout {

    private static final long serialVersionUID = 1304732129250365700L;
    private final Size size;
    // vertex of entity
    private final EntityVertex vertex; 
    // Text for entity
    private final Text entity_field;
    // Text for description
    //private final Text description_field;
    private final TextArea authHints_field;
    //private final Text description_field;
    private final TextArea messages_field;
    // Text for current path length
    private final Text trustCurrentPathLength_field;

    public EntityView(@NonNull final Size size, final EntityVertex vertex) {

      this.size = requireNonNull(size, "Missing Size Object");

      if (vertex!= null) {
        requireNonNull(vertex, "Missing Vertex component");
        this.vertex= vertex;

        // Use Text for entity
        entity_field = new Text(this.vertex.getEntity().getPlainValue());
        addFormItem(entity_field, "Entity");

        // Use Text for current path length
        if (!vertex.equals(trustGraph.getLeafVertex())) {
          trustCurrentPathLength_field = new Text(Integer.toString(this.vertex.getCurrentPathLength().get()));
          addFormItem(trustCurrentPathLength_field, "Current-Path-Length");
        } else {
          trustCurrentPathLength_field = new Text(Integer.toString(trustGraph.getMaxPathLength()));
          addFormItem(trustCurrentPathLength_field, "Max-Path-Length");
        }

        // Use Text for auth hints
        authHints_field = new TextArea();
        authHints_field.setValue(vertex.AuthorityHintsToString());
        authHints_field.setWidth(this.size.getWidth()*0.68 + this.size.getUnit());
        addFormItem(authHints_field, "Authority Hints");

          // Use Text for messages
          messages_field = new TextArea();
          messages_field.setValue(String.join("\n", vertex.getMessages()));
          messages_field.setWidth(this.size.getWidth()*0.68 + this.size.getUnit());
          addFormItem(messages_field, "Description");
      } else {
        this.vertex= null; entity_field = null; authHints_field = null; messages_field = null; trustCurrentPathLength_field = null;
      }

      setWidth(this.size.getWidthSize());
      //setHeight(this.size.getHeightSize());
      setMinWidth(this.size.getWidthSize());
      setMaxWidth(this.size.getWidthSize());
      setMaxHeight(this.size.getHeightSize());
    }
  }

  private ArrayList<String> trustGraphToStatus() {

    ArrayList<String> status = new ArrayList<String>();

    status.add("Entity;"+trustGraph.getLeaf().getPlainValue());
    status.add("Trust-Anchor-Reached;" + Boolean.toString(trustGraph.isTrustAnchorReached()));
    status.add("Trust-Chain-Verified;" + Boolean.toString(trustGraph.verificationHasSucceeded()));
    status.add("Max-Path-Length;" + Integer.toString(trustGraph.getMaxPathLength()));
    status.add("End-of-Validity;" + trustGraph.getStarted().toString());
    status.add("Started;" + trustGraph.getStarted().toString());
    status.add("Finished;" + trustGraph.getFinished().toString());
    status.add("Duration (ms);" + Long.toString(trustGraph.getDuration().toMillis()));

    return status;
  }
}