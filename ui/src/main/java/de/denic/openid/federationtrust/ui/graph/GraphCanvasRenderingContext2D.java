/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.ui.graph;

import java.io.Serializable;


public class GraphCanvasRenderingContext2D {

    private GraphCanvas canvas;

    public GraphCanvasRenderingContext2D(GraphCanvas canvas) {
        this.canvas = canvas;
    }

    public void setFillStyle(String fillStyle) {
        setProperty("fillStyle", fillStyle);
    }

    public void setStrokeStyle(String strokeStyle) {
        setProperty("strokeStyle", strokeStyle);
    }

    public void setLineWidth(double lineWidth) {
        setProperty("lineWidth", lineWidth);
    }

    public void setFont(String font) {
        setProperty("font", font);
    }

    public void arc(double x, double y, double radius, double startAngle,
            double endAngle, boolean antiClockwise) {
        callJsMethod("arc", x, y, radius, startAngle, endAngle, antiClockwise);
    }

    public void beginPath() {
        callJsMethod("beginPath");
    }

    public void clearRect(double x, double y, double width, double height) {
        callJsMethod("clearRect", x, y, width, height);
    }

    public void closePath() {
        callJsMethod("closePath");
    }

    /**
     * Fetches the image from the given location and draws it on the canvas.
     * <p>
     * <b>NOTE:</b> The drawing will happen asynchronously after the browser has
     * received the image.
     * 
     * @param src
     *            the url of the image to draw
     * @param x
     *            the x-coordinate of the top-left corner of the image
     * @param y
     *            the y-coordinate of the top-left corner of the image
     */
    public void drawImage(String src, double x, double y) {
        runScript(String.format(
        //@formatter:off
            "var img = new Image();"
          + "img.onload = function () {"
          +   "$0.getContext('2d').drawImage(img, %s, %s);"
          + "};"
          + "img.src='%s';", x, y, src));
        //@formatter:on
    }

    /**
     * Fetches the image from the given location and draws it on the canvas.
     * <p>
     * <b>NOTE:</b> The drawing will happen asynchronously after the browser has
     * received the image.
     * 
     * @param src
     *            the url of the image to draw
     * @param x
     *            the x-coordinate of the top-left corner of the image
     * @param y
     *            the y-coordinate of the top-left corner of the image
     * @param width
     *            the width for the image
     * @param height
     *            the height for the image
     */
    public void drawImage(String src, double x, double y, double width,
            double height) {
        runScript(String.format(
        //@formatter:off
            "var img = new Image();"
          + "img.onload = function () {"
          +   "$0.getContext('2d').drawImage(img, %s, %s, %s, %s);"
          + "};"
          + "img.src='%s';", x, y, width, height, src));
        //@formatter:on
    }

    public void ellipse(double x, double y, double radiusX, double radiusY,
                        double rotation, double startAngle, double endAngle , boolean antiClockwise) {
        callJsMethod("ellipse", x, y, radiusX, radiusY, rotation, startAngle, endAngle, antiClockwise);
    }

    public void fill() {
        callJsMethod("fill");
    }

    public void fillRect(double x, double y, double width, double height) {
        callJsMethod("fillRect", x, y, width, height);
    }

    public void fillText(String text, double x, double y) {
        callJsMethod("fillText", text, x, y);
    }

    public void lineTo(double x, double y) {
        callJsMethod("lineTo", x, y);
    }

    public int measureText(String text) {
        callJsMethodWithReturn("measureText", text);
        //String test = res.toString();
        //int ret = Integer.parseInt(test);
        return 0;
    }

    public void moveTo(double x, double y) {
        callJsMethod("moveTo", x, y);
    }

    public void rect(double x, double y, double width, double height) {
        callJsMethod("rect", x, y, width, height);
    }

    public void restore() {
        callJsMethod("restore");
    }

    public void rotate(double angle) {
        callJsMethod("rotate", angle);
    }

    public void save() {
        callJsMethod("save");
    }

    public void scale(double x, double y) {
        callJsMethod("scale", x, y);
    }

    public void stroke() {
        callJsMethod("stroke");
    }

    public void strokeRect(double x, double y, double width, double height) {
        callJsMethod("strokeRect", x, y, width, height);
    }

    public void strokeText(String text, double x, double y) {
        callJsMethod("strokeText", text, x, y);
    }

    public void translate(double x, double y) {
        callJsMethod("translate", x, y);
    }

    protected void setProperty(String propertyName, Serializable value) {
        runScript(String.format("$0.getContext('2d').%s='%s'", propertyName,
                value));
    }

    /**
     * Runs the given js so that the execution order works with callJsMethod().
     * Any $0 in the script will refer to the canvas element.
     */
    private void runScript(String script) {
        canvas.getElement().getNode().runWhenAttached(
                // This structure is needed to make the execution order work
                // with Element.callFunction() which is used in callJsMethod()
                ui -> ui.getInternals().getStateTree().beforeClientResponse(
                        canvas.getElement().getNode(),
                        context -> ui.getPage().executeJavaScript(script,
                                canvas.getElement())));
    }

    protected void callJsMethod(String methodName, Serializable... parameters) {
        canvas.getElement().callFunction("getContext('2d')." + methodName,
                parameters);
    }

    protected void callJsMethodWithReturn(String methodName, Serializable... parameters) {
       canvas.getElement().callJsFunction("getContext('2d')." + methodName,
                parameters).then(String.class, resultHandler -> {
                //com.vaadin.flow.internal.JsonCodec.decodeAs(json, type)
                    if (resultHandler != null) {
                        System.out.println("Width is:" + resultHandler);
                    } else {
                        System.out.println(
                            "Feature is not supported");
                    }
                }        
        );
    }
}