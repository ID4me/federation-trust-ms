/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.ui.view;

import de.denic.openid.federationtrust.business.EntityVertex;
import de.denic.openid.federationtrust.business.FedEntityStmtEdge;
import de.denic.openid.federationtrust.business.FederationTrustService;
import de.denic.openid.federationtrust.business.TrustGraph;
import de.denic.openid.federationtrust.common.Entity;
import de.denic.openid.federationtrust.common.TrustAnchor;
import de.denic.openid.federationtrust.ui.common.Size;
import de.denic.openid.federationtrust.ui.event.GraphCanvasClickEvent;
import de.denic.openid.federationtrust.ui.graph.GraphCanvas;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.button.Button;
//import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

import org.jgrapht.graph.AsUnmodifiableGraph;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.dot.DOTExporter;
import org.jgrapht.nio.Attribute;

import static java.util.Objects.requireNonNull;

import java.io.StringWriter;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.Map;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;

/**
 * The main view contains a button and a click listener.
 */
@Route("")
@PWA(name = "Project Base for Vaadin", shortName = "Project Base")
//@CssImport("./styles/shared-styles.css")
//@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
public class MainView extends VerticalLayout {

	private static final long serialVersionUID = 4360757682694015428L;

	private final FederationTrustService federationTrustService;

  // Canvas area for graph painting
  private GraphCanvas canva = null;
  // Vertex area for user out info
  private DetailsView details = null;
  private static final String FORM_WIDTH = "800px";
  private static final String FORM_HEIGHT = "500px";
  private static final String DETAILS_WIDTH = "800px";
  private static final String DETAILS_HEIGHT = "500px";
  private static final String CANVAS_WIDTH = "800px";
  private static final String CANVAS_HEIGHT = "500px";
  private static final int CANVAS_MULTIPLICATOR = 2;
  private static final int CANVAS_PADDING_X = 10;
  private static final int CANVAS_PADDING_Y = 10;

  // Input form area
  private final FormView form;    

  public MainView(@NonNull final FederationTrustService federationTrustService) {

    // create trust service
    this.federationTrustService =  requireNonNull(federationTrustService, "Missing Federation Trust Service component");
    // create form view for user input
    form = new FormView(new Size(FORM_WIDTH, FORM_HEIGHT), e -> onFormButtonClickEvent(e));
    // Use custom CSS classes to apply styling. This is defined in shared-styles.css.
    //addClassName("centered-content");

    setWidth("100%");
    setHeight("100%");
    //this.getStyle().set("border", "1px solid #9E9E9E");
    //this.setSpacing(false);
    setJustifyContentMode(JustifyContentMode.START);
    setHorizontalComponentAlignment(Alignment.CENTER, form);
    add(form);
  }

  public void getTrustGraph(@NonNull final String host, @Nullable final String trustAnchorValue, @Nullable final String trustAnchorJWKSetValue,
                            @Nullable final Integer maxPathLength) {
                                  
    // calc trust chain
    final Entity entity = Entity.ofHost(host);
    final TrustAnchor trustAnchor = TrustAnchor.fromEntityAndJWKSet(trustAnchorValue, trustAnchorJWKSetValue);
    final TrustGraph trustGraph = federationTrustService.seekForChainOf(entity)
            .withMaxPathLength(maxPathLength)
            .withCustomTrustAnchors(trustAnchor)
            .graphNow();

    // create and add canvas for graph rendering
    GraphCanvas new_canva = new GraphCanvas(new Size(CANVAS_WIDTH, CANVAS_HEIGHT), CANVAS_PADDING_X, CANVAS_PADDING_Y, CANVAS_MULTIPLICATOR, trustGraph, e -> onGraphicsCanvasClickEvent(e));        
    new_canva.getStyle().set("border", "1px solid");
    // create and add details view 
    DetailsView new_details = new DetailsView(new Size(DETAILS_WIDTH, DETAILS_HEIGHT), trustGraph);
    new_details.getStyle().set("border", "1px solid");
    //new_details.getStatusView().getStyle().set("border", "1px solid");
    clearMainView();
    setHorizontalComponentAlignment(Alignment.CENTER, new_canva);
    setHorizontalComponentAlignment(Alignment.CENTER, new_details);
    //if (trustGraph.isTrustAnchorReached() && trustGraph.verificationHasSucceeded()) {add(new_canva);};
    if (trustGraph.isTrustAnchorReached()) {add(new_canva);};
    add(new_details);
    canva = new_canva;
    details = new_details;
    JGraphtoString(trustGraph.getGraph());
  }

  public void JGraphtoString(@NonNull final AsUnmodifiableGraph<EntityVertex, FedEntityStmtEdge> graph) {
    DOTExporter<EntityVertex, FedEntityStmtEdge> exporter =
    new DOTExporter<>(v -> v.getEntity().getPlainValue().replace('-', '_').replace('.', '_').replace("/", "").replace(":",""));


    exporter.setVertexAttributeProvider((v) -> {
    Map<String, Attribute> map = new LinkedHashMap<>();
    map.put("label", DefaultAttribute.createAttribute(v.getEntity().getPlainValue()));
    return map;
    });
    Writer writer = new StringWriter();
    exporter.exportGraph(graph, writer);
    System.out.println("------------------ Print DOT Graph -----------------------------");
    System.out.println(writer.toString());
    System.out.println("------------------ Print Raw jGraphT Graph ---------------------");
    for (EntityVertex vertex : graph.vertexSet()) {
      System.out.println(vertex.getEntity().getPlainValue() + "  valid:" + vertex.isValid() );
    }
    for (FedEntityStmtEdge edge : graph.edgeSet()) {
      graph.getEdgeSource(edge);
      System.out.println(graph.getEdgeSource(edge).getEntity().getPlainValue() + " -> " +
                             graph.getEdgeTarget(edge).getEntity().getPlainValue());
    }
    System.out.println("----------------------------------------------------------------");

  }


  private void onGraphicsCanvasClickEvent(GraphCanvasClickEvent event) {
    EntityVertex vertex = canva.getVertexAtCoordinate(event.getOffsetX(), event.getOffsetY());
    if (vertex != null) {
      //Notification.show("x: " + event.getOffsetX() + " y: " + event.getOffsetY() + " Vertex: " +  vertex.getEntity().getPlainValue());
      details.addEntity(vertex);
    } else {
      //Notification.show("x: " + event.getOffsetX() + " y: " + event.getOffsetY());
      details.addEmptyEntityView();
    }
  }

  private void onFormButtonClickEvent(ClickEvent<Button> e) {
    if (form.isUserInputValid()) {
      getTrustGraph(form.entity_field.getValue(), form.trustAnchor_field.getValue(), form.trustAnchorJWK_field.getValue(),
                   form.trustMaxPathLength_field.getValue());
    }
  }

  private void clearMainView() {
    if (details!= null){remove(details);}
    if (canva!= null){remove(canva);}
  }
} 
