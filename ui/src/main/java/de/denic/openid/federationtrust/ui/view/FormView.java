/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.ui.view;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;

import de.denic.openid.federationtrust.ui.common.Size;

import static java.util.Objects.requireNonNull;

import io.micronaut.core.annotation.NonNull;

/**
 * The form view contains requires a click listener.
 */
@CssImport("./styles/shared-styles.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
public class FormView extends FormLayout {

  private static final long serialVersionUID = -7426679076927979233L;
  private Size size;
  // TextField for entity
  protected final TextField entity_field;;    
  // TextField for ta
  protected final TextField trustAnchor_field;
  // TextField for ta jwkset
  protected final TextField trustAnchorJWK_field;
  // TextField for max path length
  protected final IntegerField trustMaxPathLength_field;
  protected static final int TRUST_MAX_PATH_DEFAULT = 6;
  // Button for submit
  protected final Button button;


  public FormView(@NonNull final Size size, @NonNull final ComponentEventListener<ClickEvent<Button>> clickListener) {

    requireNonNull(size, "Missing Size Object");
    this.size = size;

    requireNonNull(clickListener, "Missing ButtonClickEvent Listener component");
    // Use TextField for entity
    entity_field = new TextField("Entity");
    entity_field.setPattern("[-.A-Za-z0-9áàăâåäãąāæćĉčċçďđéèĕêěëėęēğĝġģĥħíìĭîïĩįīıĵķĺľļłńňñņŋóòŏôöőõøōœĸŕřŗśŝšşßťţŧúùŭûůüűũųūŵýŷÿźžżðþ]+");
    entity_field.setRequired(true);
    entity_field.setRequiredIndicatorVisible(true);
    entity_field.setErrorMessage("Entity name is required");
    // Use TextField for ta
    trustAnchor_field = new TextField("Trust Anchor");
    // Use TextField for ta jwkset
    trustAnchorJWK_field = new TextField("Trust Anchor JWKSet");
    // Use IntegerField for path length
    trustMaxPathLength_field = new IntegerField("Trust Path Max Length");
    trustMaxPathLength_field.setMin(0);
    trustMaxPathLength_field.setErrorMessage("Number > 0");
    // Use Button for trust validation
    button = new Button("Verify Trust", clickListener);
    button.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    button.addClickShortcut(Key.ENTER);
    button.setMaxWidth("25%");
    // Use custom CSS classes to apply styling. This is defined in shared-styles.css.
    //addClassName("centered-content");

    //setWidth(width);
    //setHeight(height);
    setMaxWidth(this.size.getWidthSize());
    //setMaxHeight(height);
    add(entity_field, trustAnchor_field, trustAnchorJWK_field, trustMaxPathLength_field, button);
  }

  public boolean isUserInputValid() {
    boolean valid=true;
    entity_field.setInvalid(entity_field.isEmpty());
    valid = !entity_field.isEmpty() && !trustMaxPathLength_field.isInvalid();
    trustMaxPathLength_field.setValue(trustMaxPathLength_field.isEmpty()? TRUST_MAX_PATH_DEFAULT : trustMaxPathLength_field.getValue());
    return valid;
  }
}